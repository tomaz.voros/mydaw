#pragma once

#include <array>
#include <cstddef>
#include <type_traits>
#include <algorithm>

namespace std {
    inline constexpr std::ptrdiff_t dynamic_extent = -1;

    template <
        typename T,
        std::ptrdiff_t Extent = std::dynamic_extent
    >
    class span {
    public:
        using element_type = T;
        using value_type = std::remove_cv_t<T>;
        using index_type = std::ptrdiff_t;
        using difference_type = std::ptrdiff_t;
        using pointer = T *;
        using reference = T &;
        // does not satisfy ConstexprIterator
        using iterator = pointer;
        // does not satisfy ConstexprIterator
        using const_iterator = const pointer;
        using reverse_iterator = std::reverse_iterator<iterator>;
        using const_reverse_iterator = std::reverse_iterator<const_iterator>;

        static constexpr std::ptrdiff_t extent = Extent;

        constexpr span() noexcept :
            mData{ nullptr }, mSize{ 0 } {}
        constexpr span(pointer ptr, index_type count) :
            mData{ ptr }, mSize{ count } {}
        constexpr span(pointer firstElem, pointer lastElem) :
            span{ firstElem, lastElem - firstElem } {}
        template <std::size_t N>
        constexpr span(element_type (&arr) [N]) noexcept :
            mData{ std::data(arr) }, mSize{ N } {}
        template <std::size_t N>
        constexpr span(std::array<value_type, N> & arr) noexcept :
            mData{ std::data(arr) }, mSize{ N } {}
        template <std::size_t N>
        constexpr span(const std::array<value_type, N>& arr) noexcept :
            mData{ std::data(arr) }, mSize{ N } {}
        template <typename Container>
        constexpr span(Container & cont) :
            mData{ std::data(cont) }, mSize{ std::size(cont) } {}
        template <typename Container>
        constexpr span(const Container & cont) :
            mData{ std::data(cont) }, mSize{ std::size(cont) } {}
        template <typename U, std::ptrdiff_t N>
        constexpr span(const std::span<U, N> & s) noexcept :
            mData{ s.data() }, mSize{ s.size() } {}
        constexpr span(const span& other) noexcept = default;

        constexpr span & operator=(const span & other) noexcept = default;

        constexpr iterator begin() const noexcept { return mData; }
        constexpr const_iterator cbegin() const noexcept { return mData; }
        constexpr iterator end() const noexcept { return mData + mSize; }
        constexpr const_iterator cend() const noexcept { return mData + mSize; }
        // is not implemented
        constexpr iterator rbegin() const noexcept;
        constexpr const_iterator crbegin() const noexcept;
        constexpr iterator rend() const noexcept;
        constexpr const_iterator rcend() const noexcept;

        constexpr reference operator[](index_type idx) const { return mData[idx]; }
        constexpr reference operator()(index_type idx) const { return mData[idx]; }

        constexpr pointer data() const noexcept { return mData; }
        constexpr index_type size() const noexcept { return mSize; }
        constexpr index_type size_bytes() const noexcept { return mSize * sizeof(element_type); }
        constexpr bool empty() const noexcept { return mSize == 0; }

        template <std::ptrdiff_t Count>
        constexpr std::span<element_type, Count> first() const {
            return span<element_type, Count>{ mData, Count };
        }
        constexpr std::span<element_type, std::dynamic_extent> first(std::ptrdiff_t Count) const {
            return span<element_type>{ mData, Count };
        }
        template <std::ptrdiff_t Count>
        constexpr std::span<element_type, Count> last() const {
            return std::span<element_type, Count>{ mData + (mSize - Count), Count };
        }
        constexpr std::span<element_type, std::dynamic_extent> last(std::ptrdiff_t Count) const {
            return std::span<element_type>{ mData + (mSize - Count), Count };
        }

        template<
            std::ptrdiff_t Offset,
            std::ptrdiff_t Count = std::dynamic_extent
        >
        constexpr std::span<
            element_type,
            Count != std::dynamic_extent ? Count :
            (Extent != std::dynamic_extent ? Extent - Offset : std::dynamic_extent)
        > subspan() const {
            return span{ mData + Offset, Count == std::dynamic_extent ? mSize - Offset : Count };
        }
        constexpr std::span<element_type, std::dynamic_extent> 
        subspan(std::ptrdiff_t Offset, std::ptrdiff_t Count = std::dynamic_extent) const {
            return span<element_type>{ mData + Offset, Count == std::dynamic_extent ? mSize - Offset : Count };
        }

    private:
        pointer mData;
        // obsolete if Extent != std::dynamic_extent
        index_type mSize;

    };

    template <typename T, std::ptrdiff_t X, typename U, std::ptrdiff_t Y>
    constexpr bool operator==(std::span<T, X> l, std::span<U, Y> r) {
        return std::equal(l.begin(), l.end(), r.begin(), r.end());
    }
    template <typename T, std::ptrdiff_t X, typename U, std::ptrdiff_t Y>
    constexpr bool operator!=(std::span<T, X> l, std::span<U, Y> r) {
        return !(l == r);
    }
    template <typename T, std::ptrdiff_t X, typename U, std::ptrdiff_t Y>
    constexpr bool operator<(std::span<T, X> l, std::span<U, Y> r) {
        return std::lexicographical_compare(l.begin(), l.end(), r.begin(), r.end());
    }
    template <typename T, std::ptrdiff_t X, typename U, std::ptrdiff_t Y>
    constexpr bool operator<=(std::span<T, X> l, std::span<U, Y> r) {
        return !(r < l);
    }
    template <typename T, std::ptrdiff_t X, typename U, std::ptrdiff_t Y>
    constexpr bool operator>(std::span<T, X> l, std::span<U, Y> r) {
        return (r < l);
    }
    template <typename T, std::ptrdiff_t X, typename U, std::ptrdiff_t Y>
    constexpr bool operator>=(std::span<T, X> l, std::span<U, Y> r) {
        return !(l < r);
    }

    template<typename T, std::ptrdiff_t N>
    std::span<
        const std::byte,
        N == std::dynamic_extent ? std::dynamic_extent : std::ptrdiff_t(sizeof(T)) * N
    >
    as_bytes(std::span<T, N> s) noexcept {
        return { reinterpret_cast<const std::byte *>(s.data()), s.size_bytes() };
    }

    template< class T, std::ptrdiff_t N>
    std::span<
        std::byte,
        N == std::dynamic_extent ? std::dynamic_extent : std::ptrdiff_t(sizeof(T)) * N
    > as_writable_bytes(std::span<T, N> s) noexcept {
        return { reinterpret_cast<std::byte *>(s.data()), s.size_bytes() };
    }
}
