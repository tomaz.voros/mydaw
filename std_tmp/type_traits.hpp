#pragma once

// TODO: replace with <type_traits> when c++20 endianness is added
namespace std {
    enum class endian {
        little = __ORDER_LITTLE_ENDIAN__,
        big = __ORDER_BIG_ENDIAN__,
        native = __BYTE_ORDER__
    };
}
