# daw

This is a proof of concept audio recording, playback and audio mixing program
source code. A JSON file is used to construct a project. The project can be
played back and seeked. Recording and exporting is implemented but not exposed
to the user. The GUI is very minimal. I have not studied the source code of any
similar project. If you have any questions, contact me.

## Future work

* Get a name for the project
* Expose recording
* Additional audio input / output API support
* Implement proper GUI
* Allow editing project via GUI
* Allow saving project
* Support audio effects

## Screenshots:
![screenshot](assets/screenshot1.png)
