#include <catch/catch.hpp>
#include <daw/pcm.hpp>
#include <limits>
#include <cstdint>
#include <random>

TEST_CASE("simple-wav-file") {
    SECTION("opening-files") {
        std::vector<const char *> validFileNames{ {
                "assets/octaves.wav",
                "assets/aiffSine.aiff"
        } };

        for (const auto & fileName : validFileNames) {
            pcm::Clip clip{ fileName };
            REQUIRE(clip.isGood());
            const auto & meta = clip.getMeta();
            std::vector<std::byte> data;
            const auto frameSize = meta.channelCount * pcm::sampleSize(meta.format);
            data.resize(frameSize * meta.frameCount + frameSize);
            const auto framesRead2 = clip.read(data.data(), 0, meta.frameCount + 1);
            CHECK(framesRead2 == meta.frameCount);
            const auto framesRead3 = clip.read(data.data(), 0, meta.frameCount / 2);
            CHECK(framesRead3 == meta.frameCount / 2);

            const auto framesRead = clip.read(data.data(), 0, meta.frameCount);
            CHECK(framesRead == meta.frameCount);

            std::vector<float> result;
            result.resize(meta.frameCount * meta.channelCount);
            pcm::castAnySamplesToFloatNE(data.data(), meta.format, result.size(), result.data());
        }
    }

    SECTION("type-conversion-function-presence") {
        std::array<std::byte, 16> data;
        std::array<float, 16> result;
        constexpr uint64_t SAMPLE_COUNT = 1;
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::INT8_LE>     (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::INT16_LE>    (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::INT24_LE>    (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::INT32_LE>    (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::UINT8_LE>    (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::UINT16_LE>   (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::UINT24_LE>   (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::UINT32_LE>   (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::FLOAT32_LE>  (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::INT8_BE>     (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::INT16_BE>    (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::INT24_BE>    (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::INT32_BE>    (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::UINT8_BE>    (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::UINT16_BE>   (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::UINT24_BE>   (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::UINT32_BE>   (data.data(), SAMPLE_COUNT, result.data()));
        REQUIRE_NOTHROW(pcm::castSamplesToFloatNE<pcm::Format::FLOAT32_BE>  (data.data(), SAMPLE_COUNT, result.data()));
    }
}

TEST_CASE("float-inverse-operations") {
    auto testConversion = [] (int64_t from, int64_t to, float div, float mul) {
        for (int64_t i = from; i <= to; ++i) {
            const auto d = float(i) / div;
            const auto m = float(i) * mul;
            CHECK(d == m);
        }
    };

    INFO("Testing int8.");
    testConversion(-128, 127, pcm::constant::POW_2_07, pcm::constant::INV_POW_2_07);
    INFO("Testing int16.");
    testConversion(-32768, 32767, pcm::constant::POW_2_07, pcm::constant::INV_POW_2_07);
    INFO("Testing int24.");
    testConversion(-8388608, 8388607, pcm::constant::POW_2_07, pcm::constant::INV_POW_2_07);
    INFO("Testing int32.");
    testConversion(-2147483648, 2147483647, pcm::constant::POW_2_07, pcm::constant::INV_POW_2_07);
}

TEST_CASE("pcm-formats") {
    std::vector<const char *> validFileNames{ {
        "assets/440_float32_be.aiff",
        "assets/440_float32_le.wav",
        "assets/440_int16_be.aiff",
        "assets/440_int16_le.wav",
        "assets/440_int24_be.aiff",
        "assets/440_int24_le.wav",
        "assets/440_int32_be.aiff",
        "assets/440_int32_le.wav",
        "assets/440_int8_be.aiff",
        "assets/440_uint8_be.aiff",
        "assets/440_uint8_le.wav"
    } };

    for (const auto & fileName : validFileNames) {
        SECTION(fileName) {
            pcm::Clip clip{ fileName };
            REQUIRE(clip.isGood());
            const auto & meta = clip.getMeta();

            REQUIRE(meta.channelCount == 1);

            std::vector<std::byte> data;
            data.resize(meta.channelCount * meta.frameCount * pcm::sampleSize(meta.format));

            std::vector<float> result;
            result.resize(meta.frameCount * meta.channelCount);

            const auto framesRead = clip.read(data.data(), 0, meta.frameCount);
            REQUIRE(framesRead == meta.frameCount);

            REQUIRE_NOTHROW(pcm::castAnySamplesToFloatNE(data.data(), meta.format, result.size(), result.data()));

            pcm::wav::save(fileName + std::string{ ".wav" }, { result }, meta.frameRate);
        }
    }
}

TEST_CASE("signal-resampling") {
    constexpr uint64_t OUT_RATE = 20000;
    pcm::Clip clip{ "assets/440_int16_le.wav" };
    REQUIRE(clip.isGood());
    const auto & meta = clip.getMeta();
    REQUIRE(meta.frameRate != OUT_RATE);
    REQUIRE(meta.channelCount == 1);

    std::vector<std::byte> raw;
    raw.resize(meta.channelCount * meta.frameCount * pcm::sampleSize(meta.format));
    const auto framesRead = clip.read(raw.data(), 0, meta.frameCount);
    REQUIRE(framesRead == meta.frameCount);

    std::vector<float> samples;
    samples.resize(meta.frameCount * meta.channelCount);
    REQUIRE_NOTHROW(pcm::castAnySamplesToFloatNE(raw.data(), meta.format, samples.size(), samples.data()));

    pcm::resampling::Nearest resampler{ meta.frameRate, OUT_RATE };
    REQUIRE(resampler.isGood());
    std::vector<float> result;
    result.resize(meta.frameCount * 3);
    REQUIRE(result.size() >= 1000);
    resampler.nextRange(0, 1000);
    resampler.resample(samples.data(), result.data());
    result.resize(1000);
    pcm::wav::save("assets/resampled.wav", { result }, OUT_RATE);
}

TEST_CASE("continuous-resampling") {
    constexpr uint64_t OUT_RATE = 30000;
    constexpr uint64_t BUFFER_SIZE = 16;

    pcm::Clip clip{ "assets/440_int16_le.wav" };
    const auto & meta = clip.getMeta();

    std::vector<std::byte> raw;
    std::vector<float> inBuffer;
    std::vector<float> outBuffer;

    std::vector<float> resultBuffer;

    outBuffer.resize(BUFFER_SIZE);

    pcm::resampling::Nearest resampler{ meta.frameRate, OUT_RATE };

    REQUIRE(clip.isGood());
    REQUIRE(meta.frameRate != OUT_RATE);
    REQUIRE(meta.channelCount == 1);
    REQUIRE(resampler.isGood());

    uint64_t i = 0;
    while (true) {
        auto [inStart, inSize] = resampler.nextRange(i, BUFFER_SIZE);

        raw.resize(std::max(inSize * pcm::sampleSize(meta.format), raw.size()));
        inBuffer.resize(std::max(inSize, inBuffer.size()));

        const auto framesRead = clip.read(raw.data(), inStart, inSize);

        REQUIRE(((framesRead == inSize) || (inStart + framesRead == meta.frameCount)));
        if (framesRead != inSize) break; // not complete :(

        REQUIRE_NOTHROW(pcm::castAnySamplesToFloatNE(raw.data(), meta.format, inSize, inBuffer.data()));

        resampler.resample(inBuffer.data(), outBuffer.data());

        i += BUFFER_SIZE;

        std::copy(std::begin(outBuffer), std::end(outBuffer), std::back_inserter(resultBuffer));
    }

    pcm::wav::save("assets/resampled2.wav", { resultBuffer }, OUT_RATE);
}

TEST_CASE("continuous-resampling-linear") {
    constexpr uint64_t OUT_RATE = 30000;
    constexpr uint64_t BUFFER_SIZE = 16;

    pcm::Clip clip{ "assets/440_int16_le.wav" };
    const auto & meta = clip.getMeta();

    std::vector<std::byte> raw;
    std::vector<float> inBuffer;
    std::vector<float> outBuffer;

    std::vector<float> resultBuffer;

    outBuffer.resize(BUFFER_SIZE);

    pcm::resampling::Linear resampler{ meta.frameRate, OUT_RATE };

    REQUIRE(clip.isGood());
    REQUIRE(meta.frameRate != OUT_RATE);
    REQUIRE(meta.channelCount == 1);
    REQUIRE(resampler.isGood());

    uint64_t i = 0;
    while (true) {
        auto [inStart, inSize] = resampler.nextRange(i, BUFFER_SIZE);

        raw.resize(std::max(inSize * pcm::sampleSize(meta.format), raw.size()));
        inBuffer.resize(std::max(inSize, inBuffer.size()));

        const auto framesRead = clip.read(raw.data(), inStart, inSize);

        REQUIRE(((framesRead == inSize) || (inStart + framesRead == meta.frameCount)));
        if (framesRead != inSize) break; // not complete :(

        REQUIRE_NOTHROW(pcm::castAnySamplesToFloatNE(raw.data(), meta.format, inSize, inBuffer.data()));

        resampler.resample(inBuffer.data(), outBuffer.data());

        i += BUFFER_SIZE;

        std::copy(std::begin(outBuffer), std::end(outBuffer), std::back_inserter(resultBuffer));
    }

    pcm::wav::save("assets/resampled3.wav", { resultBuffer }, OUT_RATE);
}

TEST_CASE("resampler-range-function") {
    constexpr uint64_t TEST_COUNT = 10'000'000;

    constexpr uint64_t MIN_BUFFER_SIZE = 1;
    constexpr uint64_t MAX_BUFFER_SIZE = 1024 * 1024;

    constexpr uint64_t MIN_POSITION = 0;
    constexpr uint64_t MAX_POSITION = 1024 * 1024 * 1024;

    constexpr uint64_t MIN_IN_RATE = 1;
    constexpr uint64_t MAX_IN_RATE = 192'000 * 2;

    constexpr uint64_t MIN_OUT_RATE = 1;
    constexpr uint64_t MAX_OUT_RATE = 192'000 * 2;

    SECTION("linear") {
        std::mt19937 gen{ 42 };
        std::uniform_int_distribution<uint64_t> bufferSize{ MIN_BUFFER_SIZE, MAX_BUFFER_SIZE };
        std::uniform_int_distribution<uint64_t> position  { MIN_POSITION,    MAX_POSITION    };
        std::uniform_int_distribution<uint64_t> inRate    { MIN_IN_RATE,     MAX_IN_RATE     };
        std::uniform_int_distribution<uint64_t> outRate   { MIN_OUT_RATE,    MAX_OUT_RATE    };
        for (uint64_t i = 0; i < TEST_COUNT; ++i) {
            const uint64_t a = inRate(gen);
            const uint64_t b = outRate(gen);
            const uint64_t c = bufferSize(gen);
            const uint64_t d = position(gen);
            pcm::resampling::Linear resampler{ a, b };
            REQUIRE(resampler.isGood());
            const uint64_t maxRange = resampler.maxRangeSize(c);
            const uint64_t currentRange = resampler.nextRange(d, c).second;
            CHECK(maxRange >= currentRange);
        }
    }

    SECTION("nearest") {
        std::mt19937 gen{ 42 };
        std::uniform_int_distribution<uint64_t> bufferSize{ MIN_BUFFER_SIZE, MAX_BUFFER_SIZE };
        std::uniform_int_distribution<uint64_t> position  { MIN_POSITION,    MAX_POSITION    };
        std::uniform_int_distribution<uint64_t> inRate    { MIN_IN_RATE,     MAX_IN_RATE     };
        std::uniform_int_distribution<uint64_t> outRate   { MIN_OUT_RATE,    MAX_OUT_RATE    };
        for (uint64_t i = 0; i < TEST_COUNT; ++i) {
            const uint64_t a = inRate(gen);
            const uint64_t b = outRate(gen);
            const uint64_t c = bufferSize(gen);
            const uint64_t d = position(gen);
            pcm::resampling::Nearest resampler{ a, b };
            REQUIRE(resampler.isGood());
            const uint64_t maxRange = resampler.maxRangeSize(c);
            const uint64_t currentRange = resampler.nextRange(d, c).second;
            CHECK(maxRange >= currentRange);
        }
    }
}

TEST_CASE("clip-stream") {
    constexpr uint64_t OUT_RATE = 25000;
    constexpr uint64_t BUFFER_SIZE = 16;
    constexpr uint64_t COUNT = OUT_RATE * 3;

    pcm::ClipStream cs{ "assets/440_int16_le.wav", OUT_RATE };
    REQUIRE(cs.isGood());

    const auto bufferSizes = cs.bufferSizeRequirement(BUFFER_SIZE);
    const auto channelCount = cs.channelCount();

    std::vector<float> buffer;
    std::vector<float *> buffers;
    std::vector<std::byte> byteBuffer;
    std::vector<float> floatBuffer;
    std::vector<float *> floatBuffers;
    /// memory allocation
    byteBuffer.resize(bufferSizes.first);
    floatBuffer.resize(bufferSizes.second * channelCount);
    floatBuffers.reserve(channelCount);
    buffer.resize(channelCount * BUFFER_SIZE);
    buffers.reserve(channelCount);
    for (uint64_t c = 0; c < channelCount; ++c)
        floatBuffers.emplace_back(floatBuffer.data() + c * bufferSizes.second);
    for (uint64_t c = 0; c < channelCount; ++c)
        buffers.emplace_back(buffer.data() + c * BUFFER_SIZE);

    std::vector<std::vector<float>> result;
    result.resize(channelCount);

    for (uint64_t s = 0; s < COUNT; s += BUFFER_SIZE) {
        cs.read(buffers.data(), s, BUFFER_SIZE, byteBuffer.data(), floatBuffers.data());
        for (uint64_t c = 0; c < channelCount; ++c) {
            std::copy(buffers[c], buffers[c] + BUFFER_SIZE, std::back_inserter(result[c]));
        }
    }

    pcm::wav::save("assets/result4.wav", result, OUT_RATE);
}
