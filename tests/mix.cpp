#include <catch/catch.hpp>
#include <daw/mix.hpp>
#include <daw/pcm.hpp>

TEST_CASE("simple-track") {
    constexpr uint64_t CHANNEL_COUNT = 2;
    constexpr uint64_t FRAME_RATE = 48000;
    constexpr uint64_t BUFFER_SIZE = 64;
    constexpr uint64_t EXACT_RECORDED_FRAMES = BUFFER_SIZE * 2000;

    mix::Track track{ CHANNEL_COUNT, FRAME_RATE };

    REQUIRE(track.channelCount() == 2);

    std::shared_ptr<pcm::ClipStream> clip1 = std::make_shared<pcm::ClipStream>("assets/octaves.wav", FRAME_RATE);
    std::shared_ptr<pcm::ClipStream> clip2 = std::make_shared<pcm::ClipStream>("assets/440_int24_be.aiff", FRAME_RATE);
    std::shared_ptr<pcm::ClipStream> clip3 = std::make_shared<pcm::ClipStream>("assets/440_uint8_le.wav", FRAME_RATE);

    REQUIRE(clip1->isGood());
    REQUIRE(clip2->isGood());
    REQUIRE(clip3->isGood());

    mix::ClipPlacement placement1;
    placement1.clip = clip1;
    placement1.length = 10000;
    placement1.offset = 0;
    placement1.position = 0;

    mix::ClipPlacement placement2;
    placement2.clip = clip2;
    placement2.length = 10000;
    placement2.offset = 0;
    placement2.position = 15000;

    mix::ClipPlacement placement3;
    placement3.clip = clip3;
    placement3.length = 10000;
    placement3.offset = 0;
    placement3.position = 25000;

    mix::ClipPlacement placement4;
    placement4.clip = clip1;
    placement4.length = 10000;
    placement4.offset = 5000;
    placement4.position = 60000;

    mix::ClipPlacement placement5;
    placement5.clip = clip1;
    placement5.length = 10000;
    placement5.offset = 15000;
    placement5.position = 75000;

    REQUIRE(track.addClip(std::move(placement1)));
    REQUIRE(track.addClip(std::move(placement2)));
    REQUIRE(track.addClip(std::move(placement3)));
    REQUIRE(track.addClip(std::move(placement4)));
    REQUIRE(track.addClip(std::move(placement5)));

    const auto req1 = clip1->bufferSizeRequirement(BUFFER_SIZE);
    const auto req2 = clip2->bufferSizeRequirement(BUFFER_SIZE);
    const auto req3 = clip3->bufferSizeRequirement(BUFFER_SIZE);

    mem::Buffer buff;

    buff.channelCount = CHANNEL_COUNT;

    buff.frameCount = std::max(buff.frameCount, req1.second);
    buff.frameCount = std::max(buff.frameCount, req2.second);
    buff.frameCount = std::max(buff.frameCount, req3.second);
  
    buff.byteCount = std::max(buff.byteCount, req1.first);
    buff.byteCount = std::max(buff.byteCount, req2.first);
    buff.byteCount = std::max(buff.byteCount, req3.first);
    
    mem::allocateBuffer(buff);

    track.seek(0);

    std::vector<std::vector<float>> result;
    std::vector<float *> results;
    result.resize(CHANNEL_COUNT);
    for (auto & r : result)
        r.resize(EXACT_RECORDED_FRAMES);
    results.resize(CHANNEL_COUNT);

    for (uint64_t i = 0; i < EXACT_RECORDED_FRAMES; i += BUFFER_SIZE) {
        for (uint64_t c = 0; c < CHANNEL_COUNT; ++c)
            results[c] = result[c].data() + i;
        track.read(results.data(), BUFFER_SIZE, buff);
    }

    mem::deallocateBuffer(buff);

    pcm::wav::save("assets/wowout.wav", result, FRAME_RATE);
}
