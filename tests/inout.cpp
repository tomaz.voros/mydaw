#include <daw/inout.hpp>
#include <catch/catch.hpp>
#include <array>

TEST_CASE("inout-pa-simple") {
    constexpr uint64_t FRAME_RATE = 48000;
    constexpr uint64_t BUFFER_SIZE = 64;
    constexpr uint64_t APPROXIMATE_SECONDS = 5;
    constexpr uint64_t APPROXIMATE_FRAME_COUNT = FRAME_RATE * APPROXIMATE_SECONDS;
    constexpr uint64_t EXACT_FRAME_COUNT = (APPROXIMATE_FRAME_COUNT / BUFFER_SIZE + (APPROXIMATE_FRAME_COUNT % BUFFER_SIZE != 0)) * BUFFER_SIZE;
    constexpr uint64_t CHANNEL_COUNT = 2;
    constexpr pcm::Format SAMPLE_FORMAT = pcm::Format::FLOAT32_LE;

    std::array<uint64_t, CHANNEL_COUNT> channelMapping;
    for (uint64_t c = 0; c < channelMapping.size(); ++c)
        channelMapping[c] = c;

    inout::InputPA in{
        FRAME_RATE,
        channelMapping.size(),
        channelMapping.data(),
        SAMPLE_FORMAT
    };

    std::vector<std::byte> tmpBuff;
    std::vector<std::vector<float>> result;
    std::vector<float *> results;

    tmpBuff.resize(CHANNEL_COUNT * BUFFER_SIZE * pcm::sampleSize(SAMPLE_FORMAT));
    results.resize(CHANNEL_COUNT);
    result.resize(CHANNEL_COUNT);
    for (auto & r : result)
        r.resize(EXACT_FRAME_COUNT);

    auto * data = tmpBuff.data();

    in.start();
    REQUIRE(in.isGood());

    REQUIRE(in.interleaved());

    for (uint64_t frame = 0; frame < EXACT_FRAME_COUNT; frame += BUFFER_SIZE) {
        in.read(&data, BUFFER_SIZE);
        for (size_t channel = 0; channel < CHANNEL_COUNT; ++channel) {
            results[channel] = result[channel].data() + frame;
        }
        pcm::deinterleaveCastSamplesToFloatNE<SAMPLE_FORMAT>(data, BUFFER_SIZE, CHANNEL_COUNT, results.data());
    }

    pcm::wav::save("assets/outtttt.wav", result, FRAME_RATE);

    in.stop();
}
