#pragma once

#include <vector>
#include <cstdint>
#include <string>
#include <filesystem.hpp>

namespace projectFile {
    struct ClipInfo {
        uint64_t index;
        uint64_t channel;
        uint64_t position;
        uint64_t offset;
        uint64_t length;
    };

    struct Track {
        uint64_t channelCount;
        std::vector<ClipInfo> clips;
    };

    enum class Type : uint64_t {
        UNKNOWN, TRACK, EFFECT, OUTPUT
    };

    struct Socket {
        Type type;
        uint64_t index;
        uint64_t channel;
    };

    struct Connection {
        Socket from;
        Socket to;
    };

    struct EffectInstance {
        uint64_t index;
        uint64_t inputs;
        uint64_t outputs;
    };

    struct Effects {
        std::vector<std::string> types;
        std::vector<EffectInstance> instances;
    };

    struct ProjectMeta {
        uint64_t frameRate;
        std::vector<std::string> clips;
        std::vector<Track> tracks;
        Effects effects;
        std::vector<std::string> outputs;
        std::vector<Connection> connections;
    };

    ProjectMeta projectMetaFromFile(const std::filesystem::path & filePath);
    void projectMetaToFile(const std::filesystem::path & filePath, const ProjectMeta & projectMeta);

}
