#include <cassert>
#include <cstdint>
#include <atomic>
#include <GLFW/glfw3.h>
#include <OpenGL.hpp>
#include <UserInput.hpp>
#include <draw.hpp>
#include <memory>
#include <vector>
#include <iostream>
#include <thread>
#include "projectFile.hpp"
#include <mix.hpp>
#include <pcm.hpp>
#include <draw.hpp>
#include <inout.hpp>
#include <mem.hpp>

static void renderThread(
    int64_t frameRate,
    //const std::vector<std::shared_ptr<Track>> & tracks,
    std::atomic_int64_t & head,
    std::atomic_bool & run,
    std::atomic_bool & exitApp,
    std::vector<std::vector<mix::ClipPlacement>> clipPlacementCopy
) {
    if (frameRate < 1) return;
    if (glfwInit() != GLFW_TRUE) return;
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    GLFWwindow * window = glfwCreateWindow(1280, 720, "myDaw", nullptr, nullptr);
    if (window == nullptr) return;
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    OpenGL gl;

    UserInput input{ window };

    draw::Root root{ gl, (uint64_t)frameRate, { 4, 4 }, 120 };

    for (uint64_t t = 0; t < clipPlacementCopy.size(); ++t)
        for (auto & c : clipPlacementCopy[t])
            root.addClip(gl, t, c.position, c.length, c.offset, *c.clip.get());

    int fbW, fbH;
    glfwGetFramebufferSize(window, &fbW, &fbH);

    static constexpr char PLAYING_TEXT[] = "playing";
    static constexpr char PAUSED_TEXT[] = "paused";

    bool audioRunning = run.load();
    if (audioRunning)
        glfwSetWindowTitle(window, PLAYING_TEXT);
    else
        glfwSetWindowTitle(window, PAUSED_TEXT);


    while (!glfwWindowShouldClose(window)) {
        //glfwWaitEventsTimeout(1. / 10.);
        glfwPollEvents();
        //std::cout << glfwGetTime() << std::endl;
        for (auto e = input.getPointerEvent(); e.second; e = input.getPointerEvent()) {
            auto ee = e.first;
        //    std::cout << "e:" << std::endl;
        //    std::cout << "   " << ee.position.x << ", " << ee.position.y << std::endl;
        //    std::cout << "   " << ee.movement.x << ", " << ee.movement.y << std::endl;
            ee.position.y = fbH - ee.position.y - 1;
            ee.movement.y = -ee.movement.y;
            root.event(ee);
        }
        // std::cout << "r: " << head << std::endl;

        bool togglePlay = false;
        for (auto e = input.getKeyEvent(); e.second; e = input.getKeyEvent()) {
            auto ee = e.first;
            // std::cout << ee.key << std::endl;
            if (ee.key == GLFW_KEY_SPACE) {
                togglePlay = !togglePlay;
            }
            
            // TODO: check this, there are probably dataraces despite atomic
            if (!audioRunning) {
                if (ee.key == GLFW_KEY_LEFT) {
                    head = std::max(int64_t(0), head - frameRate);
                } else if (ee.key == GLFW_KEY_RIGHT) {
                    head += frameRate;
                }
            }
        }

        if (togglePlay) {
            audioRunning = !audioRunning;
            run.store(audioRunning);

            if (audioRunning)
                glfwSetWindowTitle(window, PLAYING_TEXT);
            else
                glfwSetWindowTitle(window, PAUSED_TEXT);
        }

        root.setHead(head.load());

        gl.clearColor(1.0f, 0.0f, 1.0f, 1.0f);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        //std::cout << '*';
        //std::cout.flush();
        int fbWidth, fbHeight;
        glfwGetFramebufferSize(window, &fbWidth, &fbHeight);
        glutil::viewport vp;
        vp.w = fbWidth;
        vp.h = fbHeight;

        fbW = fbWidth;
        fbH = fbHeight;

        math::i32v2 screenSize;
        screenSize.x = fbWidth;
        screenSize.y = fbHeight;

        gl.viewport(vp.x, vp.y, vp.w, vp.h);
        root.resize(vp, screenSize);
        root.draw(gl);

        if (const auto x = gl.getError(); x != 0) std::cout << x << std::endl;
        glfwSwapBuffers(window);
    }

    run = false;
    exitApp = true;
}

static void audioThread(
    std::atomic_int64_t & head,
    std::atomic_bool & run,
    std::atomic_bool & exitApp,
    std::vector<mix::Track> & tracks,
    uint64_t frameRate,
    float gain
) {
    constexpr uint64_t BUFFER_SIZE = 64;
    constexpr std::array<uint64_t, 1> inputChannelIndices{ { 0 } };

    if (tracks.size() < 1) return;

    constexpr std::array<uint64_t, 2> channelIndices{ { 0, 1 } };
    inout::OutputPA out{ frameRate, channelIndices.size(), channelIndices.data(), pcm::Format::FLOAT32_LE };
    const uint64_t channelCount = channelIndices.size();
    const uint64_t sampleSize = pcm::sampleSize(out.sampleFormat());

    struct TrackData {
        mem::Buffer scrapBuffer;
        std::vector<float> buffer;
        std::vector<float *> channels;
    };

    std::vector<TrackData> trackData;
    trackData.reserve(tracks.size());

    for (const auto & track : tracks) {
        auto & data = trackData.emplace_back();

        data.scrapBuffer = track.memoryRequirement(BUFFER_SIZE);
        mem::allocateBuffer(data.scrapBuffer);

        data.buffer.resize(track.channelCount() * BUFFER_SIZE);

        data.channels.reserve(track.channelCount());
        for (size_t i = 0; i < track.channelCount(); ++i)
            data.channels.emplace_back(data.buffer.data() + BUFFER_SIZE * i);
    }

    std::vector<float> mixBuffer;
    mixBuffer.resize(channelCount * BUFFER_SIZE);
    std::vector<float *> mixBuffers;
    mixBuffers.reserve(channelCount);
    for (size_t i = 0; i < channelCount; ++i)
        mixBuffers.emplace_back(mixBuffer.data() + BUFFER_SIZE * i);

    std::vector<float> outBuffer;
    outBuffer.resize(channelCount * BUFFER_SIZE);

    // TODO: implement recording
    //       before starting to play, check if one should record, and it yes on what channel
    //       then before playback start the input and stop input on pause
    inout::InputPA in{ frameRate, inputChannelIndices.size(), inputChannelIndices.data(), pcm::Format::FLOAT32_LE };

    // number of samples, the value should depend on the sampling rate
    static constexpr int64_t START_FADE_LENGTH = 100;
    static constexpr int64_t STOP_FADE_LENGTH = 100;
    static constexpr bool USE_START_FADE = true;
    static constexpr bool USE_STOP_FADE = true;

    while (!exitApp) {
        while (!run.load() && !exitApp.load())
            std::this_thread::sleep_for(std::chrono::milliseconds{ 100 });

        // there are some data races with head position
        for (auto & t : tracks)
            t.seek(head.load());

        out.start();
        if (!out.isGood()) return;
        if (!out.interleaved()) return;

        int64_t fadeInLeft = START_FADE_LENGTH;
        int64_t fadeOutLeft = STOP_FADE_LENGTH;

        while (run.load() || fadeOutLeft > 0) {
            // read tracks
            for (size_t i = 0; i < tracks.size(); ++i)
                tracks[i].read(trackData[i].channels.data(), BUFFER_SIZE, trackData[i].scrapBuffer);

            // mix tracks
            std::fill(std::begin(mixBuffer), std::end(mixBuffer), 0.0f);
            for (size_t t = 0; t < tracks.size(); ++t) {
                const size_t effectiveChannelCount = std::min(
                    trackData[t].channels.size(),
                    channelCount
                );
                for (size_t c = 0; c < effectiveChannelCount; ++c)
                    for (size_t f = 0; f < BUFFER_SIZE; ++f)
                        mixBuffers[c][f] += trackData[t].channels[c][f];
            }

            if (USE_START_FADE) {
                // do fade in if just started
                const int64_t thisLoopFadeInLength = std::min<int64_t>(fadeInLeft, BUFFER_SIZE);
                for (int64_t s = 0; s < thisLoopFadeInLength; ++s) {
                    const int64_t fadeInIndex = START_FADE_LENGTH - fadeInLeft + s;
                    assert(fadeInIndex >= 0 && fadeInIndex < START_FADE_LENGTH);
                    const float a = float(fadeInIndex) / float(START_FADE_LENGTH);
                    for (size_t c = 0; c < channelCount; ++c)
                        mixBuffers[c][s] *= a;
                }
                fadeInLeft -= thisLoopFadeInLength;
            }

            if (USE_STOP_FADE && !run.load()) {
                // do fade out if just stopped
                const int64_t thisLoopFadeOutLength = BUFFER_SIZE;
                for (int64_t s = 0; s < thisLoopFadeOutLength; ++s) {
                    const int64_t fadeOutIndex = STOP_FADE_LENGTH - fadeOutLeft + s;
                    assert(fadeOutIndex >= 0);
                    const float a = 1.0f - float(std::min<int64_t>(fadeOutIndex, STOP_FADE_LENGTH)) / float(STOP_FADE_LENGTH);
                    for (size_t c = 0; c < channelCount; ++c)
                        mixBuffers[c][s] *= a;
                }
                fadeOutLeft -= std::min<int64_t>(fadeOutLeft, BUFFER_SIZE);
            }

            // interleave
            mem::interleave(mixBuffers.data(), outBuffer.data(), channelCount, BUFFER_SIZE);

            // apply gain
            for (auto & s : outBuffer) s *= gain;

            const float * res = outBuffer.data();
            out.write(reinterpret_cast<const std::byte * *>(&res), BUFFER_SIZE);

            head += BUFFER_SIZE;
        }
        out.wait();
        out.stop();
    }

    for (auto & track : trackData)
        mem::deallocateBuffer(track.scrapBuffer);
}

int main() {
    // open project file
    const auto projectMeta = projectFile::projectMetaFromFile("assets/testProject5.json");

    if (projectMeta.frameRate < 1)
        return -1;

    // load clips
    std::vector<std::shared_ptr<pcm::ClipStream>> clips;
    clips.reserve(projectMeta.clips.size());
    for (const auto & c : projectMeta.clips)
        clips.emplace_back(std::make_shared<pcm::ClipStream>(c, projectMeta.frameRate));
    
    for (const auto & c : clips)
        if (!c->isGood())
            return -1;

    std::vector<std::vector<mix::ClipPlacement>> clipPlacementCopy;
    clipPlacementCopy.resize(projectMeta.tracks.size());

    // load tracks
    std::vector<mix::Track> tracks;
    tracks.reserve(projectMeta.tracks.size());
    for (const auto & t : projectMeta.tracks)
        auto & tr = tracks.emplace_back(t.channelCount, projectMeta.frameRate);
    for (size_t i = 0; i < tracks.size(); ++i) {
        for (auto & c : projectMeta.tracks[i].clips) {
            mix::ClipPlacement clipPlacement;
            if (c.index >= clips.size())
                return -1;
            clipPlacement.clip = clips[c.index];
            clipPlacement.position = c.position;
            clipPlacement.length = c.length;
            clipPlacement.offset = c.offset;
            // TODO: deprecate c.channel
            clipPlacementCopy[i].push_back(clipPlacement); // copy
            const auto addSuccess = tracks[i].addClip(std::move(clipPlacement));
            if (!addSuccess)
                return -1;
        }
    }

    std::atomic_int64_t head = 0;
    std::atomic_bool run = false;
    std::atomic_bool exitApp = false;

    // start render
    auto renderer = std::thread{
        &renderThread,
        projectMeta.frameRate,
        //std::cref(tracks),
        std::ref(head),
        std::ref(run),
        std::ref(exitApp),
        std::ref(clipPlacementCopy)
    };

    // TODO: synchronization (renderer must not read clips for processing while audio thread is playing)
    //       for now, use must not press play as long while the renderer is reading clips

    //start audio
    audioThread(head, run, exitApp, tracks, projectMeta.frameRate, 0.3f);

    renderer.join();
    return 0;
}
