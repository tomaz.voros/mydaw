#include "projectFile.hpp"

#include <fstream>
#include <iomanip>
#include <nlohmann/json.hpp>
#include <iostream>

namespace projectFile {
    /*
    void to_json(json & j, const X & o);
    void from_json(const json & j, X & o);
    */

    static void from_json(const nlohmann::json & j, ProjectMeta & o) {
        o.frameRate = j.at("frameRate").get<decltype(o.frameRate)>();
        o.clips = j.at("clips").get<decltype(o.clips)>();
        o.tracks = j.at("tracks").get<decltype(o.tracks)>();
        o.effects = j.at("effects").get<decltype(o.effects)>();
        o.outputs = j.at("outputs").get<decltype(o.outputs)>();
        o.connections = j.at("connections").get<decltype(o.connections)>();
    }

    static void from_json(const nlohmann::json & j, EffectInstance & o) {
        o.index = j.at("index").get<decltype(o.index)>();
        o.inputs = j.at("inputs").get<decltype(o.inputs)>();
        o.outputs = j.at("outputs").get<decltype(o.outputs)>();
    }

    static void from_json(const nlohmann::json & j, Effects & o) {
        o.types = j.at("types").get<decltype(o.types)>();
        o.instances = j.at("instances").get<decltype(o.instances)>();
    }

    static void from_json(const nlohmann::json & j, Track & o) {
        o.channelCount = j.at("channelCount").get<decltype(o.channelCount)>();
        o.clips = j.at("clips").get<decltype(o.clips)>();
    }

    static void from_json(const nlohmann::json & j, ClipInfo & o) {
        o.index = j.at("index").get<decltype(o.index)>();
        o.channel = j.at("channel").get<decltype(o.channel)>();
        o.position = j.at("position").get<decltype(o.position)>();
        o.offset = j.at("offset").get<decltype(o.offset)>();
        o.length = j.at("length").get<decltype(o.length)>();
    }

    static void from_json(const nlohmann::json & j, Connection & o) {
        o.from = j.at("from").get<decltype(o.from)>();
        o.to = j.at("to").get<decltype(o.to)>();
    }

    static void from_json(const nlohmann::json & j, Socket & o) {
        std::string type = j.at("type").get<std::string>();
        if (type == "track")
            o.type = Type::TRACK;
        else if (type == "effect")
            o.type = Type::EFFECT;
        else if (type == "output")
            o.type = Type::OUTPUT;
        else
            o.type = Type::UNKNOWN;
        o.index = j.at("index").get<decltype(o.index)>();
        o.channel = j.at("channel").get<decltype(o.channel)>();
    }
}

projectFile::ProjectMeta projectFile::projectMetaFromFile(const std::filesystem::path & filePath) {
    std::ifstream projectFile{ filePath };
    nlohmann::json projectJson;
    projectFile >> projectJson;
    ProjectMeta result = projectJson;
    return result;
}

void projectFile::projectMetaToFile(const std::filesystem::path & filePath, const ProjectMeta & projectMeta) {
    throw std::runtime_error("Not implemented.");
}
