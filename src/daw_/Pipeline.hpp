#pragma once

#include <memory>
#include "filesystem.hpp"
#include "Clip.hpp"
#include "Track.hpp"
#include "Output.hpp"
#include <atomic>

namespace PipelineDetail {
    struct ClipInfo {
        uint_t index;
        uint_t channel;
        uint_t position;
        uint_t offset;
        uint_t length;
    };

    // TODO: rename to Track
    struct Track2 {
        std::vector<ClipInfo> clips;
    };

    enum class Type : uint_t {
        UNKNOWN, TRACK, EFFECT, OUTPUT
    };

    struct Socket {
        Type type;
        uint_t index;
        uint_t channel;
    };

    struct Connection {
        Socket from;
        Socket to;
    };

    struct EffectInstance {
        uint_t index;
        uint_t inputs;
        uint_t outputs;
    };

    struct Effects {
        std::vector<std::string> types;
        std::vector<EffectInstance> instances;
    };

    struct ProjectMeta {
        uint_t frameRate;
        std::vector<std::string> clips;
        std::vector<Track2> tracks;
        Effects effects;
        std::vector<std::string> outputs;
        std::vector<Connection> connections;
    };
}

class Pipeline {
public:
    Pipeline(const std::filesystem::path & projectPath);
    void run(std::atomic_int64_t & head, std::atomic_bool & run, bool record, const std::filesystem::path & recordingFileName);

    uint_t frameRate() const { return mFrameRate; }

    // TODO: more elegant way to access state (or share state)
    const std::vector<std::shared_ptr<Track>> & tracks() const { return mTracks; }

private:
    uint_t mFrameRate{ 0 };
    uint_t mBufferSize{ 64 };
    std::vector<std::shared_ptr<Clip>> mClips;
    std::vector<std::shared_ptr<Track>> mTracks;
    std::vector<std::shared_ptr<Effect>> mEffects;
    std::vector<std::shared_ptr<Output>> mOutputs;

    void setupSchedule(const PipelineDetail::ProjectMeta & projectMeta);

    struct Schedule {
        // track->effect
        struct TrackEffect {
            std::vector<samp_t> buffer;
            std::shared_ptr<Track> track;
            uint_t trackIndex;
            std::shared_ptr<Effect> effect;
            uint_t effectIndex;
            uint_t effectChannel;
        };
        // track->output
        struct TrackOutput {
            std::vector<samp_t> buffer;
            std::shared_ptr<Track> track;
            uint_t trackIndex;
            std::shared_ptr<Output> output;
            uint_t outputIndex;
        };
        // effect->effect
        struct EffectEffect {
            // TODO: move buffers to effects instead of shattered everywhere
            std::vector<samp_t> buffer;
            std::shared_ptr<Effect> fromEffect;
            uint_t fromEffextIndex;
            std::shared_ptr<Effect> toEffect;
            uint_t toEffextIndex;
            uint_t fromEffectChannel;
            uint_t toEffectChannel;
        };
        // effect->output
        struct EffectOutput {
            // TODO: move buffers to effects instead of shattered everywhere
            std::vector<samp_t> buffer;
            std::shared_ptr<Effect> effect;
            uint_t effectIndex;
            std::shared_ptr<Output> output;
            uint_t outputIndex;
            uint_t effectChannel;
        };

        std::vector<TrackEffect> trackEffect;
        std::vector<TrackOutput> trackOutput;
        std::vector<EffectEffect> effectEffect;
        std::vector<EffectOutput> effectOutput;
        std::vector<uint_t> eeOrder;
        std::vector<
            std::pair<
                std::vector<samp_t *>,
                std::vector<samp_t *>
            >
        > effectBuffers;
    };

    Schedule mSchedule;

};