#include "Pipeline.hpp"
#include "Types.hpp"
#include <vector>
#include <string>
#include <nlohmann/json.hpp>
#include <fstream>
#include "utility.hpp"
#include "EffectGain.hpp"
#include "EffectTanh.hpp"
#include <iostream>
#include <algorithm>
#include <set>

void Pipeline::setupSchedule(const PipelineDetail::ProjectMeta & projectMeta) {
    for (const auto & connection : projectMeta.connections) {
        if (connection.from.type == PipelineDetail::Type::TRACK && connection.to.type == PipelineDetail::Type::EFFECT) {
            if (connection.from.index >= mTracks.size())
                throw std::runtime_error("Track index out of bounds.");
            if (connection.to.index >= mEffects.size())
                throw std::runtime_error("Effect index out of bounds.");
            if (connection.to.channel >= projectMeta.effects.instances[connection.to.index].inputs)
                throw std::runtime_error("Effect input out of bounds.");
            auto & c = mSchedule.trackEffect.emplace_back();
            c.buffer.resize(mBufferSize);
            c.track = mTracks[connection.from.index];
            c.trackIndex = connection.from.index;
            c.effect = mEffects[connection.to.index];
            c.effectIndex = connection.to.index;
            c.effectChannel = connection.to.channel;
        } else if (connection.from.type == PipelineDetail::Type::TRACK && connection.to.type == PipelineDetail::Type::OUTPUT) {
            if (connection.from.index >= mTracks.size())
                throw std::runtime_error("Track index out of bounds.");
            if (connection.to.index >= mOutputs.size())
                throw std::runtime_error("Output index out of bounds.");
            auto & c = mSchedule.trackOutput.emplace_back();
            c.buffer.resize(mBufferSize);
            c.track = mTracks[connection.from.index];
            c.trackIndex = connection.from.index;
            c.output = mOutputs[connection.to.index];
            c.outputIndex = connection.to.index;
        } else if (connection.from.type == PipelineDetail::Type::EFFECT && connection.to.type == PipelineDetail::Type::EFFECT) {
            if (connection.from.index >= mEffects.size())
                throw std::runtime_error("Effect index out of bounds.");
            if (connection.to.index >= mEffects.size())
                throw std::runtime_error("Effect index out of bounds.");
            if (connection.from.channel >= projectMeta.effects.instances[connection.from.index].inputs)
                throw std::runtime_error("Effect input out of bounds.");
            if (connection.to.channel >= projectMeta.effects.instances[connection.to.index].outputs)
                throw std::runtime_error("Effect output out of bounds.");
            auto & c = mSchedule.effectEffect.emplace_back();
            c.buffer.resize(mBufferSize);
            c.fromEffect = mEffects[connection.from.index];
            c.fromEffextIndex = connection.from.index;
            c.toEffect = mEffects[connection.to.index];
            c.toEffextIndex = connection.to.index;
            c.fromEffectChannel = connection.from.channel;
            c.toEffectChannel = connection.to.channel;
        } else if (connection.from.type == PipelineDetail::Type::EFFECT && connection.to.type == PipelineDetail::Type::OUTPUT) {
            if (connection.from.index >= mEffects.size())
                throw std::runtime_error("Effect index out of bounds.");
            if (connection.to.index >= mOutputs.size())
                throw std::runtime_error("Output index out of bounds.");
            if (connection.from.channel >= projectMeta.effects.instances[connection.from.index].outputs)
                throw std::runtime_error("Effect output out of bounds.");
            auto & c = mSchedule.effectOutput.emplace_back();
            c.buffer.resize(mBufferSize);
            c.effect = mEffects[connection.from.index];
            c.effectIndex = connection.from.index;
            c.output = mOutputs[connection.to.index];
            c.outputIndex = connection.to.index;
            c.effectChannel = connection.from.channel;
        } else {
            throw std::runtime_error("Invalid connection combination.");
        }
    }

    std::vector<bool> trackOuts; // used track outs
    trackOuts.resize(mTracks.size(), false);
    std::vector<bool> outputIns; // used output ins
    outputIns.resize(mOutputs.size(), false);

    // effectInstance(inputs, outputs)
    std::vector<std::pair<std::set<uint_t>, std::set<uint_t>>> effectIos;
    effectIos.reserve(projectMeta.effects.instances.size());
    for (const auto & eff : projectMeta.effects.instances) {
        auto & e = effectIos.emplace_back();
        for (uint_t i = 0; i < eff.inputs; ++i) e.first.insert(i);
        for (uint_t i = 0; i < eff.outputs; ++i) e.second.insert(i);
    }

    // process tracks to effects
    for (uint_t i = 0; i < mSchedule.trackEffect.size(); ++i) {
        const auto & t = mSchedule.trackEffect[i];
        auto & e = effectIos[t.effectIndex];
        if (e.first.find(t.effectChannel) == e.first.end())
            throw std::runtime_error("Duplicate or out of bounds connections track->effect.");
        e.first.erase(t.effectChannel);
        if (trackOuts[t.trackIndex] != false)
            throw std::runtime_error("Track output already used.");
        trackOuts[t.trackIndex] = true;
    }

    // process tracks to outputs
    for (uint_t i = 0; i < mSchedule.trackOutput.size(); ++i) {
        const auto & t = mSchedule.trackOutput[i];
        if (trackOuts[t.trackIndex] != false)
            throw std::runtime_error("Track output already used.");
        trackOuts[t.trackIndex] = true;
        if (outputIns[t.outputIndex] != false)
            throw std::runtime_error("Output in already used.");
        outputIns[t.outputIndex] = true;
    }

    std::vector<uint_t> order;
    order.reserve(projectMeta.effects.instances.size());

    // process effects to outputs
    for (uint_t i = 0; i < mSchedule.effectOutput.size(); ++i) {
        const auto & e = mSchedule.effectOutput[i];
        auto & eio = effectIos[e.effectIndex];
        if (eio.second.find(e.effectChannel) == eio.second.end())
            throw std::runtime_error("Duplicate or out of bounds connections effect->output.");
        eio.second.erase(e.effectChannel);
        if (outputIns[e.outputIndex] != false)
            throw std::runtime_error("Output in already used.");
        outputIns[e.outputIndex] = true;
    }

    // process effect to effect
    std::vector<bool> effectProcessed;
    effectProcessed.resize(effectIos.size(), false);
    std::vector<bool> eeConnectionProcessed;
    eeConnectionProcessed.resize(mSchedule.effectEffect.size(), false);
    while (true) {
        bool newEffectProcessed = false;
        for (uint_t i = 0; i < mSchedule.effectEffect.size(); ++i) {
            auto & ee = mSchedule.effectEffect[i];
            if (eeConnectionProcessed[i]) continue;
            auto & eFrom = effectIos[ee.fromEffextIndex];
            auto & eTo = effectIos[ee.toEffextIndex];
            if (eFrom.second.find(ee.fromEffectChannel) == eFrom.second.end())
                throw std::runtime_error("Duplicate or out of bounds connections effect->effect 1.");
            if (eTo.first.find(ee.toEffectChannel) == eTo.second.end())
                throw std::runtime_error("Duplicate or out of bounds connections effect->effect 2.");
            if (effectProcessed[ee.fromEffextIndex]) {
                eFrom.second.erase(ee.fromEffectChannel);
                eTo.first.erase(ee.toEffectChannel);
                newEffectProcessed = true;
                eeConnectionProcessed[i] = true;
            }
        }

        for (uint_t i = 0; i < effectIos.size(); ++i) {
            auto & e = effectIos[i];
            if (e.first.size() > 0) continue;
            if (effectProcessed[i]) continue;
            effectProcessed[i] = true;
            order.push_back(i);
            newEffectProcessed = true;
        }

        if (order.size() == mEffects.size()) break;
        if (!newEffectProcessed) break;
    }

    if (order.size() != mEffects.size())
        throw std::runtime_error("Internal scheduling setup error 1.");
    auto checkOrder = order;
    std::sort(checkOrder.begin(), checkOrder.end());
    for (uint_t i = 0; i < checkOrder.size(); ++i)
        if (checkOrder[i] != i)
            throw std::runtime_error("Internal scheduling setup error 2.");

    for (const auto & t : trackOuts)
        if (t != true)
            throw std::runtime_error("Unused track.");

    for (const auto & eio : effectIos)
        if (eio.first.size() > 0)
            throw std::runtime_error("Unused effect input.");
        else if (eio.second.size() > 0)
            throw std::runtime_error("Unused effect output.");

    for (const auto & o : outputIns)
        if (o != true)
            throw std::runtime_error("Unused output.");
    
    mSchedule.eeOrder = std::move(order);

    mSchedule.effectBuffers.reserve(mEffects.size());
    for (const auto & e : projectMeta.effects.instances) {
        auto & p = mSchedule.effectBuffers.emplace_back();
        p.first.resize(e.inputs, nullptr);
        p.second.resize(e.outputs, nullptr);
    }
    for (auto & te : mSchedule.trackEffect) {
        auto & e =  mSchedule.effectBuffers[te.effectIndex].first[te.effectChannel];
        if (e != nullptr) throw std::runtime_error("Somehow reassigning buffer 1.");
        e = te.buffer.data();
    }
    for (auto & ee : mSchedule.effectEffect) {
        auto & e1 =  mSchedule.effectBuffers[ee.fromEffextIndex].second[ee.fromEffectChannel];
        auto & e2 =  mSchedule.effectBuffers[ee.toEffextIndex].first[ee.toEffextIndex];
        if (e1 != nullptr) throw std::runtime_error("Somehow reassigning buffer 2.");
        if (e2 != nullptr) throw std::runtime_error("Somehow reassigning buffer 3.");
        e1 = ee.buffer.data();
        e2 = ee.buffer.data();
    }
    for (auto & eo : mSchedule.effectOutput) {
        auto & e =  mSchedule.effectBuffers[eo.effectIndex].second[eo.effectChannel];
        if (e != nullptr) throw std::runtime_error("Somehow reassigning buffer 4.");
        e = eo.buffer.data();
    }

    for (const auto & p : mSchedule.effectBuffers) {
        for (const auto & pp : p.first)
            if (pp == nullptr)
                throw std::runtime_error("Missing buffer 1.");
        for (const auto & pp : p.second)
            if (pp == nullptr)
                throw std::runtime_error("Missing buffer 2.");
    }
}

Pipeline::Pipeline(const std::filesystem::path & projectPath) {
    std::ifstream projectFile{ projectPath };
    nlohmann::json projectJson;
    projectFile >> projectJson;
    PipelineDetail::ProjectMeta projectMeta = projectJson;

    // TODO: validation

    mFrameRate = projectMeta.frameRate;

    mClips.reserve(projectMeta.clips.size());
    for (const auto & clipFileName : projectMeta.clips)
        mClips.push_back(std::make_shared<Clip>(clipFileName));

    for (const auto & clip : mClips)
        if (clip->frameRate() != mFrameRate)
            throw std::runtime_error("Resampling not implemented yet, clips sample rates are mismatched.");

    for (const auto & trackIn : projectMeta.tracks) {
        mTracks.push_back(std::make_shared<Track>(
            mFrameRate,
            mBufferSize
        ));
        auto & track = mTracks.back();
        for (const auto & clipInfo : trackIn.clips) {
            track->add(
                mClips[clipInfo.index],
                clipInfo.channel,
                clipInfo.position,
                clipInfo.offset,
                clipInfo.length
            );
        }
    }

    mEffects.reserve(projectMeta.effects.instances.size());
    for (const auto & effectIn : projectMeta.effects.instances) {
        if (effectIn.index >= projectMeta.effects.types.size())
            throw std::runtime_error("Effect index out of bounds.");
        // TODO: dynamic effect loading
        const auto & effectSrc = projectMeta.effects.types[effectIn.index];
        if (effectSrc == "assets/gain.so") {
            if (effectIn.inputs != effectIn.outputs)
                throw std::runtime_error("Mismatched in/out count.");
            mEffects.push_back(std::make_shared<EffectGain>(
                effectIn.inputs,
                mBufferSize
            ));
        } else if (effectSrc == "assets/tanh.so") {
            if (effectIn.inputs != effectIn.outputs)
                throw std::runtime_error("Mismatched in/out count.");
            mEffects.push_back(std::make_shared<EffectTanh>(
                effectIn.inputs,
                mBufferSize
            ));
        } else {
            throw std::runtime_error("Unknown effect.");
        }
    }

    mOutputs.reserve(projectMeta.outputs.size());
    for (const auto & outIn : projectMeta.outputs) {
        mOutputs.push_back(std::make_unique<Output>(mFrameRate , mBufferSize));
    }

    setupSchedule(projectMeta);
}

void Pipeline::run(std::atomic_int64_t & head, std::atomic_bool & run, bool record, const std::filesystem::path & recordingFileName) {
    const auto startingPosition = head.load();
    for (auto & track : mTracks) {
        track->seek(startingPosition);
    }

    const uint_t length = (*std::max_element(
        std::begin(mTracks),
        std::end(mTracks),
        [] (const std::shared_ptr<Track> & a, const std::shared_ptr<Track> & b) -> bool {
            return a->trackEnd() < b->trackEnd();
        }
    ))->trackEnd();

    const uint_t end = utility::math::roundToNextMultiple<uint_t>(length, mBufferSize);

    std::vector<std::vector<samp_t>> backupOut;

    if (record) {
        backupOut.resize(mOutputs.size());
        for (auto & bo : backupOut)
            bo.reserve(end);
    }

    for (auto & output : mOutputs)
        output->start();

    uint_t position = startingPosition;
    for (; position < end; position += mBufferSize) {
        // in
        for (auto & to : mSchedule.trackOutput)
            to.track->read(to.buffer.data());
        for (auto & te : mSchedule.trackEffect)
            te.track->read(te.buffer.data());

        // effects
        for (const auto & i : mSchedule.eeOrder)
            mEffects[i]->apply(
                mSchedule.effectBuffers[i].first.data(),
                mSchedule.effectBuffers[i].second.data(),
                nullptr // TODO: support param * *
            );

        // backup out
        if (record) {
            for (auto & to : mSchedule.trackOutput)
                std::copy(to.buffer.data(), to.buffer.data() + mBufferSize, std::back_inserter(backupOut[to.outputIndex]));
            for (auto & eo : mSchedule.effectOutput)
                std::copy(eo.buffer.data(), eo.buffer.data() + mBufferSize, std::back_inserter(backupOut[eo.outputIndex]));
        }

        // out
        for (auto & to : mSchedule.trackOutput)
            to.output->write(to.buffer.data());
        for (auto & eo : mSchedule.effectOutput)
            eo.output->write(eo.buffer.data());

        // TODO: maybe do not set head and check run every loop because of atomic overhead
        head = static_cast<int64_t>(position);
        if (!run) break;
    }
    head = static_cast<int64_t>(position);

    for (auto & output : mOutputs)
        output->wait();
    
    for (auto & output : mOutputs)
        output->stop();

    if (record)
        utility::wav::save(recordingFileName, backupOut, mFrameRate);
    
    // cleanup
    for (auto & effect : mEffects) {
        effect->reset();
    }
}

namespace PipelineDetail {
    void from_json(const nlohmann::json & j, ProjectMeta & o) {
        o.frameRate = j.at("frameRate").get<decltype(o.frameRate)>();
        o.clips = j.at("clips").get<decltype(o.clips)>();
        o.tracks = j.at("tracks").get<decltype(o.tracks)>();
        o.effects = j.at("effects").get<decltype(o.effects)>();
        o.outputs = j.at("outputs").get<decltype(o.outputs)>();
        o.connections = j.at("connections").get<decltype(o.connections)>();
    }

    void from_json(const nlohmann::json & j, EffectInstance & o) {
        o.index = j.at("index").get<decltype(o.index)>();
        o.inputs = j.at("inputs").get<decltype(o.inputs)>();
        o.outputs = j.at("outputs").get<decltype(o.outputs)>();
    }

    void from_json(const nlohmann::json & j, Effects & o) {
        o.types = j.at("types").get<decltype(o.types)>();
        o.instances = j.at("instances").get<decltype(o.instances)>();
    }

    void from_json(const nlohmann::json & j, Track2 & o) {
        o.clips = j.at("clips").get<decltype(o.clips)>();
    }

    void from_json(const nlohmann::json & j, ClipInfo & o) {
        o.index = j.at("index").get<decltype(o.index)>();
        o.channel = j.at("channel").get<decltype(o.channel)>();
        o.position = j.at("position").get<decltype(o.position)>();
        o.offset = j.at("offset").get<decltype(o.offset)>();
        o.length = j.at("length").get<decltype(o.length)>();
    }

    void from_json(const nlohmann::json & j, Connection & o) {
        o.from = j.at("from").get<decltype(o.from)>();
        o.to = j.at("to").get<decltype(o.to)>();
    }

    void from_json(const nlohmann::json & j, Socket & o) {
        std::string type = j.at("type").get<std::string>();
        if (type == "track")
            o.type = Type::TRACK;
        else if (type == "effect")
            o.type = Type::EFFECT;
        else if (type == "output")
            o.type = Type::OUTPUT;
        else
            o.type = Type::UNKNOWN;
        o.index = j.at("index").get<decltype(o.index)>();
        o.channel = j.at("channel").get<decltype(o.channel)>();
    }
}
