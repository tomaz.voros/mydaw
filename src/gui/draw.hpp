#pragma once

#include <random>
#include <cstdint>

#include "OpenGL.hpp"
#include "glutil.hpp"
#include "UserInput.hpp"
#include "timing.hpp"
#include <pcm.hpp>

namespace draw {
    struct Clip {
        Clip(const OpenGL & gl, pcm::ClipStream & clip, int64_t position, int64_t length, int64_t offset, const math::f32v4 & color);

        void draw(
            const OpenGL & gl,
            const math::i32v2 & position,
            const math::i32v2 & size,
            double scale,
            int32_t uOffset,
            int32_t uSize,
            int32_t uIndexOffset
        ) const;

        int64_t position = 0;
        int64_t length = 0;
        int64_t offset = 0;
        math::f32v4 color{ 1.0f, 1.0f, 1.0f, 1.0f };
        static constexpr int64_t SAMPLES_PER_BIN = 100;

        uint32_t VAO = 0;
        uint32_t VBO = 0;
        int64_t sampleCount = 0;
        int64_t channelCount = 0;

    };
    
    class Arrangement {
    public:
        Arrangement(const OpenGL & gl, uint64_t sampleRate, const timing::TimeSignature & timeSignature, int64_t tempo);
        void resize(const glutil::viewport & newViewport, const math::i32v2 & screenSize);
        void draw(const OpenGL & gl);
        void sliderEvent(const UserInput::Event & event);
        void trackEvent(const UserInput::Event & event);
        void event(const UserInput::Event & event);
    //    void setTracks(const std::vector<std::shared_ptr<Track>> & tracks);
        void addClip(const OpenGL & gl, uint64_t trackIndex, uint64_t start, uint64_t length, uint64_t offset, pcm::ClipStream & clip);
        void setHead(int64_t head);

    private:
        struct Track {
            std::vector<Clip> clips;
        };

        math::i32v2 mScreenSize;
        // TODO: refactor, remove unused, make constants constexpr
        glutil::viewport mViewport;
        //std::vector<std::shared_ptr<ClipVisual>> mClipVisuals;
        std::vector<Track> mTracks;
        math::f32v4 mTrackColor{ 0.8f, 0.8f, 0.8f, 1.0f };
        int32_t mTrackBorderWidth{ 4 };
        math::f32v4 mTrackBorderColor{ 0.1f, 0.1f, 0.1f, 1.0f };
        int32_t mTrackHeight = 120;
        math::i32v4 mClipBorderWidths{ mTrackHeight / 4, 0, 0, 1 };
        // math::f32v4 mClipBorderColor{ 0.3f, 0.3f, 0.3f, 1.0f };
        int32_t mPadding = 2;

        // TODO: use int64_t for everything as long as there is no good reason not to use it

        uint32_t mClipShader = 0;
        int32_t mUOffset = -1;
        int32_t mUSize = -1;
        int32_t mUColor = -1;
        int32_t mUDimensions = -1;
        uint32_t mVAO = 0;
        uint32_t mVBO = 0;

        uint32_t mRenderFrom = 0;
        uint32_t mRenderScale = 1;
        int64_t mRenderOffset = 0;
        uint32_t mScaleChangeSpeed = 1;
        uint64_t mSampleRate = 0;
        int64_t mHeadPosition = 0;

        std::mt19937 mColorGen{ 42 };
        std::uniform_real_distribution<float> mColorDist{ 0.5f, 1.0f };

        int64_t mSliderHeight = 20;
        math::f32v4 mSliderColor{ 0.6f, 0.6f, 0.6f, 1.0f };
        int64_t mSliderBorderWidth = 8;
        math::f32v4 mSliderBorderColor{ 0.1f, 0.1f, 0.1f, 1.0f };

        timing::TimeSignature mTimeSignature;
        int64_t mTempo = 60;

        uint32_t mClipWaveShader = 0;
        int32_t mUWaveOffset = -1;
        int32_t mUWaveSize = -1;
        int32_t mUWaveColor = -1;
        int32_t mUWaveDimensions = -1;
        int32_t mUWaveIndexOffset = -1;

        int64_t mRenderFromSample = 0; /**< First sample to render. */
        int64_t mRenderLength = 0; /**< Number of samples to render. */
        // TODO: change to int64_t
        uint32_t mRenderEnd = 1; /**< 1 past the last sample. */

        double mZoomSpeed = 0.01;
    };

    class Root {
    public:
        Root(const OpenGL & gl, uint64_t sampleRate, const timing::TimeSignature & timeSignature, int64_t tempo);
        void resize(const glutil::viewport & newViewport, const math::i32v2 & screenSize);
        void draw(const OpenGL & gl);
        void event(const UserInput::Event & event);
        //void setTracks(const std::vector<std::shared_ptr<Track>> & tracks);
        void setHead(int64_t head);
        void addClip(const OpenGL & gl, uint64_t trackIndex, uint64_t start, uint64_t length, uint64_t offset, pcm::ClipStream & clip);

    private:
        glutil::viewport mViewport;
        Arrangement mArrangement;
        math::i32v2 mScreenSize;

        static constexpr int32_t BORDER = 10;
        math::f32v4 mClear{ 1.0f, 0.5f, 0.0f, 1.0f };

    };
}
