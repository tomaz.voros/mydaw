#include "UserInput.hpp"
#include <iostream>
#include <GLFW/glfw3.h>
#include <cmath>

UserInput::UserInput(GLFWwindow * window) {
    glfwSetWindowUserPointer(window, this);

    glfwSetKeyCallback(window, UserInput::keyCallback);
    glfwSetMouseButtonCallback(window, UserInput::mouseButtonCallback);
    glfwSetCursorPosCallback(window, UserInput::cursorPosCallback);
    glfwSetScrollCallback(window, UserInput::scrollCallback);

    // TODO: properly initialize so there are no weird situations in the beginning (to the actual state)
    double xPosition, yPosition;
    glfwGetCursorPos(window, &xPosition, &yPosition);
    UserInput::cursorPosCallback(window, xPosition, yPosition);

}

void UserInput::keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods) {
    UserInput * input = static_cast<UserInput *>(glfwGetWindowUserPointer(window));

    if (action == GLFW_PRESS && key == GLFW_KEY_ESCAPE) {
        glfwSetWindowShouldClose(window, GL_TRUE);
        return;
    }

    if (action != GLFW_PRESS)
        return;
    
    if (key < 0 || key > GLFW_KEY_LAST)
        return;

    KeyEvent event;
    event.key = key;
    event.mods = mods;
    input->mKeyEvents.push(event);
}

void UserInput::mouseButtonCallback(GLFWwindow * window, int button, int action, int mods) {
    UserInput * input = static_cast<UserInput *>(glfwGetWindowUserPointer(window));
    if (button != GLFW_MOUSE_BUTTON_LEFT)
        return;
    if (action == GLFW_PRESS) {
        input->mPressStart = input->mCursor;
        input->mHolding = true;
        input->mDragging = false;
        input->mInitialModifiers = mods;
    } else if (action == GLFW_RELEASE) {
        if (!input->mDragging) {
            if (
                input->mPressStart.x != input->mCursor.x ||
                input->mPressStart.y != input->mCursor.y
            )
                std::cout << "Something is wrong with input 1, maybe system uses sub-pixel cursor position which is not supported." << std::endl;

            Event event;
            event.position = input->mPressStart;
            event.movement = { 0, 0 };
            event.accumulatedMovement = { 0, 0 };
            event.initialModifiers = input->mInitialModifiers;
            input->mEvents.push(event);
        } else {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        }
        input->mHolding = false;
        input->mDragging = false;
        input->mInitialModifiers = 0;
    }
}

void UserInput::cursorPosCallback(GLFWwindow * window, double xPos, double yPos) {
    UserInput * input = static_cast<UserInput *>(glfwGetWindowUserPointer(window));

    input->mCursor.x = static_cast<int32_t>(std::floor(xPos));
    input->mCursor.y = static_cast<int32_t>(std::floor(yPos));
    //std::cout << "m: " << input->mCursor.x << " | " << input->mCursor.y << std::endl;

    if (!input->mHolding)
        return;

    if (!input->mDragging) {
        input->mDragging = true;
        input->mLastDragPos = input->mPressStart;
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }

    if (
        input->mLastDragPos.x == input->mCursor.x &&
        input->mLastDragPos.y == input->mCursor.y
    ) {
        std::cout << "Something is wrong with input 2, maybe system uses sub-pixel cursor position which is not supported." << std::endl;
    } else {
        Event event;
        event.position = input->mPressStart;
        event.movement = {
            input->mCursor.x - input->mLastDragPos.x,
            input->mCursor.y - input->mLastDragPos.y
        };
        event.accumulatedMovement = {
            input->mCursor.x - input->mPressStart.x,
            input->mCursor.y - input->mPressStart.y
        };
        event.initialModifiers = input->mInitialModifiers;
        input->mEvents.push(event);
    }

    input->mLastDragPos = input->mCursor;
}

void UserInput::scrollCallback(GLFWwindow * window, double xOffset, double yOffset) {
    UserInput * input = static_cast<UserInput *>(glfwGetWindowUserPointer(window));
}

std::pair<UserInput::Event, bool> UserInput::getPointerEvent() {
    std::pair<UserInput::Event, bool> result{ {}, false };
    if (mEvents.size() > 0) {
        result.first = mEvents.front();
        result.second = true;
        mEvents.pop();
    }
    return result;
}

std::pair<UserInput::KeyEvent, bool> UserInput::getKeyEvent() {
    std::pair<KeyEvent, bool> result{ {}, false };
    if (mKeyEvents.size() > 0) {
        result.first = mKeyEvents.front();
        result.second = true;
        mKeyEvents.pop();
    }
    return result;
}
