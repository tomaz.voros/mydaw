#pragma once

struct GLFWwindow;
#include "math.hpp"
#include <queue>
#include <utility>

class UserInput {
public:
    UserInput(GLFWwindow * window);

    /**
     * If movement.x == 0 && movement.y == 0 then the event is a click.
     * Else the event is dragging with the initial start at position and movement is the drag dx and dy.
     */
    struct Event {
        // TODO: improvement:
        //     3 event types: press, drag, release
        //     widget can then choose what to do on mouse down (aka. press) and wether drag / release have any effect
        math::i32v2 position;
        math::i32v2 movement;
        math::i32v2 accumulatedMovement;
        int initialModifiers = 0;
        bool isClick() const { return movement.x == 0 && movement.y == 0; }
    };

    struct KeyEvent {
        int key = 0;
        int mods = 0;
    };

    std::pair<Event, bool> getPointerEvent();
    std::pair<KeyEvent, bool> getKeyEvent();

private:
    static void mouseButtonCallback(GLFWwindow * window, int button, int action, int mods);
    static void cursorPosCallback(GLFWwindow * window, double x_pos, double y_pos);
    static void keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods);
    static void scrollCallback(GLFWwindow * window, double x_offset, double y_offset);

    math::i32v2 mCursor;
    math::i32v2 mPressStart;
    math::i32v2 mLastDragPos;
    int mInitialModifiers = 0;
    bool mHolding = false;
    bool mDragging = false;

    std::queue<Event> mEvents;
    std::queue<KeyEvent> mKeyEvents;

};
