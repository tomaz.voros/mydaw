#include "draw.hpp"
#include <array>
#include <GLFW/glfw3.h>
#include <algorithm>
#include <cassert>

draw::Arrangement::Arrangement(const OpenGL & gl, uint64_t sampleRate, const timing::TimeSignature & timeSignature, int64_t tempo) :
    mSampleRate{ sampleRate },
    mScaleChangeSpeed{ (uint32_t)std::max(uint64_t(1), uint64_t(sampleRate / 400u)) },
    mTimeSignature{ timeSignature },
    mTempo{ tempo }
{
    mClipWaveShader = glutil::compileProgram(
        gl,
        {
            { "shader/wave.vert", gl.VERTEX_SHADER },
            { "shader/wave.frag", gl.FRAGMENT_SHADER },
        }
    );

    const auto waveUniforms = glutil::uniformLocations(
        gl,
        mClipWaveShader,
        { "uOffset", "uSize", "uColor", "uDimensions", "uIndexOffset" }
    );

    mUWaveOffset = waveUniforms[0];
    mUWaveSize = waveUniforms[1];
    mUWaveColor = waveUniforms[2];
    mUWaveDimensions = waveUniforms[3];
    mUWaveIndexOffset = waveUniforms[4];

    mClipShader = glutil::compileProgram(
        gl,
        {
            { "shader/quad.vert", gl.VERTEX_SHADER },
            { "shader/quad.frag", gl.FRAGMENT_SHADER },
        }
    );

    const auto uniforms = glutil::uniformLocations(
        gl,
        mClipShader,
        { "uOffset", "uSize", "uColor", "uDimensions" }
    );

    mUOffset = uniforms[0];
    mUSize = uniforms[1];
    mUColor = uniforms[2];
    mUDimensions = uniforms[3];

    constexpr std::array<int32_t, 4 * 2> QUAD{ {
        0, 0, 0, 1, 1, 0, 1, 1
    } };

    gl.genVertexArrays(1, &mVAO);
    gl.genBuffers(1, &mVBO);
    gl.bindVertexArray(mVAO);
    gl.bindBuffer(gl.ARRAY_BUFFER, mVBO);
    gl.bufferData(gl.ARRAY_BUFFER, QUAD.size() * sizeof(QUAD[0]), QUAD.data(), gl.STATIC_DRAW);
    gl.vertexAttribPointer(0, 2, gl.INT, gl.FALSE, 2 * sizeof(QUAD[0]), 0);
    gl.enableVertexAttribArray(0);
    gl.bindVertexArray(0);
}

draw::Clip::Clip(
    const OpenGL & gl,
    pcm::ClipStream & clip,
    int64_t position,
    int64_t length,
    int64_t offset,
    const math::f32v4 & color
) :
    position{ position },
    length{ length },
    offset{ offset },
    color{ color }
{
    // TODO: do not generate a separate visual for every instance of the clip placement
    // TODO: reuse buffers instead of reallocating
    std::vector<std::vector<uint8_t>> visual;
    std::vector<float> buff;
    std::vector<float *> buffs;
    std::vector<float> clipFloatBuff;
    std::vector<float*> clipFloatPBuff;
    std::vector<std::byte> clipByteBuff;

    const auto clipMeta = clip.getClipMeta();
    visual.resize(clipMeta.channelCount);
    const auto clipLength = math::positiveCeilDiv(clipMeta.frameCount, clipMeta.frameRate);
    const auto clipFrames = clipLength * clipMeta.frameRate;
    buff.resize(SAMPLES_PER_BIN * clipMeta.channelCount);
    buffs.resize(clipMeta.channelCount);
    const auto memRequirement = clip.bufferSizeRequirement(SAMPLES_PER_BIN);
    clipFloatBuff.resize(memRequirement.second * clipMeta.channelCount);
    clipByteBuff.resize(memRequirement.first);
    clipFloatPBuff.resize(clipMeta.channelCount);
    for (size_t i = 0; i < clipFloatPBuff.size(); ++i)
        clipFloatPBuff[i] = clipFloatBuff.data() + memRequirement.second * i;
    for (size_t i = 0; i < buffs.size(); ++i)
        buffs[i] = buff.data() + SAMPLES_PER_BIN * i;
    for (uint64_t i = 0; i < clipFrames; i += SAMPLES_PER_BIN) {
        clip.read(buffs.data(), i, SAMPLES_PER_BIN, clipByteBuff.data(), clipFloatPBuff.data());
        for (size_t c = 0; c < clipMeta.channelCount; ++c) {
            const auto begin = buff.begin() + SAMPLES_PER_BIN * c;
            float maxVal = *std::max_element(
                begin,
                begin + SAMPLES_PER_BIN,
                [] (float a, float b) { return std::abs(a) < std::abs(b); }
            );
            maxVal = std::abs(maxVal);
            maxVal = std::clamp(maxVal, 0.0f, 1.0f);
            // TODO: reserve (or resize) the vectors in advance
            visual[c].push_back(std::round(maxVal * std::numeric_limits<uint8_t>::max()));
        }
    }

    // assert that all channels have the same length

    if (visual.size() < 1)
        return;

    gl.genVertexArrays(1, &VAO);
    gl.genBuffers(1, &VBO);
    gl.bindVertexArray(VAO);
    gl.bindBuffer(gl.ARRAY_BUFFER, VBO);
    gl.bufferData(gl.ARRAY_BUFFER, sizeof(visual[0][0]) * visual.size() * visual[0].size(), nullptr, gl.STATIC_DRAW);
    for (size_t c = 0; c < clipMeta.channelCount; ++c) {
        gl.bufferSubData(
            gl.ARRAY_BUFFER,
            c * visual[0].size() * sizeof(visual[0][0]),
            visual[c].size() * sizeof(visual[c][0]), visual[c].data()
        );
    }
    gl.vertexAttribPointer(0, 1, gl.UNSIGNED_INT, gl.TRUE, 1 * sizeof(visual[0][0]), 0);
    gl.enableVertexAttribArray(0);
    gl.bindVertexArray(0);

    sampleCount = visual[0].size();
    channelCount = visual.size();
}

void draw::Arrangement::addClip(const OpenGL & gl, uint64_t trackIndex, uint64_t start, uint64_t length, uint64_t offset, pcm::ClipStream & clip) {
    if (trackIndex >= mTracks.size())
        mTracks.resize(trackIndex + 1);

    math::f32v4 clipColor;
    clipColor.x = mColorDist(mColorGen);
    clipColor.y = mColorDist(mColorGen);
    clipColor.z = mColorDist(mColorGen);
    clipColor.w = mColorDist(mColorGen);

    mTracks[trackIndex].clips.emplace_back(
        gl, clip, start, length, offset, clipColor
    );

    mRenderEnd = std::max(uint64_t(mRenderEnd), start + length);
    mRenderScale = std::max(int64_t(1), int64_t(mSampleRate));
    mRenderLength = mRenderEnd;
}

/*
void Arrangement::setTracks(const std::vector<std::shared_ptr<Track>> & tracks) {
    mRenderFrom = 0;
    mRenderEnd = 0;
    mTracks.clear();
    mClipVisuals.clear();
    mTracks.reserve(tracks.size());
    for (const auto & t : tracks) {
        auto & track = mTracks.emplace_back();
        for (const auto & c : t->clips()) {
            auto & clip = track.clips.emplace_back();
            clip.start = c.position;
            clip.length = c.length;
            clip.color.x = mColorDist(mColorGen);
            clip.color.y = mColorDist(mColorGen);
            clip.color.z = mColorDist(mColorGen);
            clip.color.w = mColorDist(mColorGen);
            clip.visual = c.clip->getVisual();
            clip.channel = c.channel;
            mRenderEnd = std::max(uint_t(mRenderEnd), c.position + c.length);
        }
    }
    mRenderScale = std::max(int64_t(1), int64_t(mSampleRate));
}
*/

void draw::Arrangement::sliderEvent(const UserInput::Event & event) {
    if (event.isClick())
        return;

    bool doScale = false;
    bool doOffset = false;

    if (event.initialModifiers == GLFW_MOD_CONTROL) {
        doScale = true;
    } else if (event.initialModifiers == GLFW_MOD_SHIFT) {
        doOffset = true;
    } else {
        doOffset = true;
        doScale = true;
    }

    if (doOffset) {
        const int32_t arrangementWidth = mViewport.w;
        const double arrangementScale = static_cast<double>(mRenderLength) / static_cast<double>(arrangementWidth);
        mRenderFromSample += static_cast<int64_t>(std::round(static_cast<double>(-event.movement.x) * arrangementScale));
        mRenderFromSample = std::clamp<int64_t>(mRenderFromSample, 0, mRenderEnd);
    }
    if (doScale) {
        const double zoomAmount = static_cast<double>(event.movement.y) * mZoomSpeed;
        const int64_t zoomAdd = static_cast<int64_t>(std::round(static_cast<double>(mRenderLength) * zoomAmount));
        if (event.movement.y < 0)
            mRenderLength += std::min<int64_t>(zoomAdd, -1);
        else
            mRenderLength += std::max<int64_t>(zoomAdd, 1);
        mRenderLength = std::clamp<int64_t>(mRenderLength, 1, mRenderEnd);

        // TODO: improve behaviour by clamping event.accumulatedMovement.x, so that it feels like the mouse is always grabbing the same point
        double center = static_cast<double>(event.position.x + event.accumulatedMovement.x - mViewport.x) / static_cast<double>(mViewport.w);
        // should actually not be possible for the result to be outside of [0,1]
        center = std::clamp(center, 0.0, 1.0);
        mRenderFromSample -= static_cast<int64_t>(std::round(static_cast<double>(zoomAdd * center)));
        mRenderFromSample = std::clamp<int64_t>(mRenderFromSample, 0, mRenderEnd);
    }
}

void draw::Arrangement::trackEvent(const UserInput::Event & event) {
    // TODO
}

void draw::Arrangement::event(const UserInput::Event & event) {
    math::i64v2 pos;
    pos.x = event.position.x;
    pos.y = event.position.y;

    const auto a = event.position.x >= mViewport.x;
    const auto b = event.position.x < mViewport.x + mViewport.w;
    const auto c = event.position.y >= mViewport.y + mViewport.h - mSliderHeight;
    const auto d = event.position.y < mViewport.y + mViewport.h;

    if (a && b && c && d)
        sliderEvent(event);

    const auto e = event.position.y >= mViewport.y;
    const auto f = event.position.y < mViewport.y + mViewport.h - mSliderHeight;

    if (a && b && e && f)
        trackEvent(event);
}

void draw::Clip::draw(
    const OpenGL & gl,
    const math::i32v2 & position,
    const math::i32v2 & size,
    double scale,
    int32_t uOffset,
    int32_t uSize,
    int32_t uIndexOffset
) const {
    gl.bindVertexArray(VAO);
    const int32_t channelHeight = size.y / channelCount;
    for (int64_t c = 0; c < channelCount; ++c) {
        const int32_t sampleOffset = c * sampleCount;
        gl.uniform2f(uOffset, position.x, position.y + channelHeight * c);
        gl.uniform2f(uSize, float(size.x) / float(sampleCount), channelHeight);
        gl.uniform1i(uIndexOffset, sampleOffset);
        gl.drawArrays(gl.LINE_STRIP, sampleOffset, sampleCount);
    }
}

void draw::Arrangement::draw(const OpenGL & gl) {
    // TODO: record draw calls and compile in to a single draw call
    auto quad = [this, &gl] (
        const math::i32v2 & start,
        const math::i32v2 & size,
        const math::f32v4 & color
    ) {
        gl.uniform2f(mUOffset, start.x, start.y);
        gl.uniform2f(mUSize, size.x, size.y);
        gl.uniform4f(mUColor, color.x, color.y, color.z, color.w);
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
    };

    // values in pixel
    const int32_t arrangementWidth = mViewport.w;
    const int32_t sliderHeight = mSliderHeight;
    const int32_t sliderBottomMargin = mSliderBorderWidth;
    const int32_t trackCount = mTracks.size();
    const int32_t trackHeight = mTrackHeight;
    const int32_t verticalSpaceBetweenTracks = mTrackBorderWidth;
    const math::f32v4 arrangementColor{ 0.8f, 0.8f, 0.8f, 1.0f };
    const math::f32v4 spacerColor{ 0.5f, 0.5f, 0.5f, 1.0f };
    const math::f32v4 headColor{ 0, 0, 0, 1 };
    const math::f32v4 trackSpacerColor{ spacerColor };
    const int32_t headWidth = 1;
    const math::f32v4 barColor{ spacerColor };
    const math::i32v2 sliderPosition{
        mViewport.x,
        mViewport.y + mViewport.h - sliderHeight
    };
    const math::i32v2 sliderSize{
        arrangementWidth,
        sliderHeight
    };
    const math::f32v4 sliderColor = mSliderColor;
    const int32_t betweenTracksSpaces = std::max<int32_t>(0, trackCount - 1);
    const int32_t arrangementHeight = 
        sliderHeight + sliderBottomMargin +
        trackCount * trackHeight +
        betweenTracksSpaces * verticalSpaceBetweenTracks;
    const math::i32v2 arrangementPosition{
        mViewport.x,
        mViewport.y + mViewport.h - arrangementHeight
    };
    const math::i32v2 arrangementSize{
        arrangementWidth, arrangementHeight
    };
    const math::i32v2 spacerPosition{
        sliderPosition.x,
        sliderPosition.y - sliderBottomMargin
    };
    const math::i32v2 spacerSize{
        sliderSize.x,
        sliderBottomMargin
    };

    gl.useProgram(mClipShader);
    gl.bindVertexArray(mVAO);
    gl.uniform2f(mUDimensions, mScreenSize.x, mScreenSize.y);

    // arrangement
    quad(arrangementPosition, arrangementSize, arrangementColor);

    // spacers
    quad(spacerPosition, spacerSize, spacerColor);
    for (int32_t trackIndex = 0; trackIndex < betweenTracksSpaces; ++trackIndex) {
        const math::i32v2 trackSpacerPosition{
            spacerPosition.x,
            spacerPosition.y - spacerSize.y - (verticalSpaceBetweenTracks + trackHeight) * trackIndex - trackHeight + verticalSpaceBetweenTracks
        };
        const math::i32v2 trackSpacerSize{
            spacerSize.x,
            verticalSpaceBetweenTracks
        };
        quad(trackSpacerPosition, trackSpacerSize, trackSpacerColor);
    }

    struct ClipRenderRecording {
        size_t trackIndex = 0;
        size_t clipIndex = 0;
        math::i32v2 position;
        math::i32v2 size;
    };

    // TODO: avoid memory allocations
    std::vector<ClipRenderRecording> clipRenderRecordings;

    // clips
    const double arrangementScale = static_cast<double>(mRenderLength) / static_cast<double>(arrangementWidth);
    if (arrangementSize.x > 0) {
        // TODO: only loop over the visible subrange of tracks and clips
        for (int64_t trackIndex = 0; trackIndex < mTracks.size(); ++trackIndex) {
            for (int64_t clipIndex = 0; clipIndex < mTracks[trackIndex].clips.size(); ++clipIndex) {
                const auto & clip = mTracks[trackIndex].clips[clipIndex];
                assert(clip.length > 0);
                int64_t clipStart = static_cast<int64_t>(std::round(static_cast<double>(clip.position - mRenderFromSample) / arrangementScale));
                int64_t clipEnd = static_cast<int64_t>(std::round(static_cast<double>(clip.position + clip.length - mRenderFromSample) / arrangementScale));
                clipStart = std::clamp<int64_t>(clipStart, 0, arrangementSize.x - 1) + arrangementPosition.x;
                clipEnd = std::clamp<int64_t>(clipEnd, 0, arrangementSize.x) + arrangementPosition.x;
                // not sure if necessary, should assert >= 0 instead
                const int64_t clipLength = std::max<int64_t>(clipEnd - clipStart, 0);
                if (clipLength < 1)
                    continue;
                const math::i32v2 clipPosition{
                    static_cast<int32_t>(clipStart),
                    spacerPosition.y - spacerSize.y - (verticalSpaceBetweenTracks + trackHeight) * trackIndex - trackHeight + 2 * verticalSpaceBetweenTracks
                };
                const math::i32v2 clipSize{
                    clipLength,
                    trackHeight
                };
                ClipRenderRecording recording;
                recording.trackIndex = trackIndex;
                recording.clipIndex = clipIndex;
                recording.position = clipPosition;
                recording.size = clipSize;
                clipRenderRecordings.push_back(recording);
                quad(clipPosition, clipSize, clip.color);
            }
        }
    }

    // bars
    const double framesPerBeat = static_cast<double>(mSampleRate) * 240.0 / (static_cast<double>(mTempo) * static_cast<double>(mTimeSignature.lower));
    // an not good enough approximation (use lcm to find exact beat positions)
    const int64_t framesPerBeatInt = static_cast<int64_t>(framesPerBeat);
    const int64_t frameOffset = -mRenderFromSample;
    const int64_t from = mRenderFromSample / framesPerBeatInt + (mRenderFromSample % framesPerBeatInt != 0);
    // this code is not overflow safe and can cause a live lock
    for (int64_t i = from; true; ++i) {
        const double barHorizontalPosition = std::round((-mRenderFromSample + framesPerBeat * static_cast<double>(i)) / arrangementScale) + static_cast<double>(arrangementPosition.x);
        if (static_cast<int64_t>(barHorizontalPosition) >= arrangementPosition.x + arrangementSize.x)
            break;
        const math::i32v2 barPosition{
            barHorizontalPosition,
            arrangementPosition.y
        };
        const math::i32v2 barSize{
            1, arrangementHeight
        };
        quad(barPosition, barSize, barColor);
    }

    // head
    const math::i32v2 headSize{
        headWidth,
        arrangementHeight
    };
    const int64_t headPositionSample = mHeadPosition;
    assert(arrangementWidth > 0);
    const int64_t headPositionHorizontal = 
        static_cast<int64_t>(std::round(static_cast<double>(headPositionSample - mRenderFromSample) / arrangementScale)) + arrangementPosition.x;
    if (headPositionHorizontal >= arrangementPosition.x && headPositionHorizontal - arrangementPosition.x < arrangementWidth) {
        const math::i32v2 headPosition{
            static_cast<int32_t>(headPositionHorizontal),
            arrangementPosition.y
        };
        quad(headPosition, headSize, headColor);
    }

    gl.useProgram(mClipWaveShader);
    gl.uniform2f(mUWaveDimensions, mScreenSize.x, mScreenSize.y);
    gl.uniform4f(mUWaveColor, 0, 0, 0, 1);
    // clip waves
    for (const auto & c : clipRenderRecordings) {
        const auto & clip = mTracks[c.trackIndex].clips[c.clipIndex];
        clip.draw(gl, c.position, c.size, arrangementScale, mUWaveOffset, mUWaveSize, mUWaveIndexOffset);
    }
}

void draw::Arrangement::resize(const glutil::viewport & newViewport, const math::i32v2 & screenSize) {
    mViewport = newViewport;
    mScreenSize = screenSize;
    // TODO: adjust mRenderLength
}

void draw::Arrangement::setHead(int64_t head) {
    mHeadPosition = head;
}

void draw::Root::addClip(const OpenGL & gl, uint64_t trackIndex, uint64_t start, uint64_t length, uint64_t offset, pcm::ClipStream & clip) {
    mArrangement.addClip(gl, trackIndex, start, length, offset, clip);
}


draw::Root::Root(const OpenGL & gl, uint64_t sampleRate, const timing::TimeSignature & timeSignature, int64_t tempo) :
    mArrangement{ gl, sampleRate, timeSignature, tempo }
{}

void draw::Root::resize(const glutil::viewport & newViewport, const math::i32v2 & screenSize) {
    mViewport = newViewport;
    mScreenSize = screenSize;
    { // set arrangement viewport
        auto vp = newViewport;
        if (vp.w < BORDER * 2) {
            vp.w = 0;
        } else {
            vp.x += BORDER;
            vp.w -= BORDER * 2;
        }
        if (vp.h < BORDER * 2) {
            vp.h = 0;
        } else {
            vp.y += BORDER;
            vp.h -= BORDER * 2;
        }
        mArrangement.resize(vp, screenSize);
    }
}
void draw::Root::draw(const OpenGL & gl) {
    mArrangement.draw(gl);
}

void draw::Root::event(const UserInput::Event & event) {
    mArrangement.event(event);
}
/*
void Root::setTracks(const std::vector<std::shared_ptr<Track>> & tracks) {
    mArrangement.setTracks(tracks);
}
*/
void draw::Root::setHead(int64_t head) {
    mArrangement.setHead(head);
}
