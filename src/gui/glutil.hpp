#pragma once

#include <vector>
#include <utility>
#include <string>
#include "OpenGL.hpp"

namespace glutil {
    // TODO: use python script to convert glsl files to cpp strings
    uint32_t compileProgram(
        const OpenGL & gl,
        const std::vector<std::pair<std::string, uint32_t>> & source
    );

    std::vector<int32_t> uniformLocations(
        const OpenGL & gl,
        uint32_t program,
        const std::vector<std::string> & names
    );

    struct viewport {
        int32_t x = 0, y = 0, w = 0, h = 0;
    };
}
