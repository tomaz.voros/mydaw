#include "glutil.hpp"
#include <stdexcept>
#include <fstream>
#include <iostream>
#include <array>

uint32_t glutil::compileProgram(
    const OpenGL & gl,
    const std::vector<std::pair<std::string, uint32_t>> & source
) {
    const uint32_t program = gl.createProgram();

    std::vector<uint32_t> shaders;
    shaders.reserve(source.size());
    for (const auto & s : source) {
        std::ifstream file{ s.first };
        const std::string sourceText{ std::istreambuf_iterator<char>{ file }, std::istreambuf_iterator<char>{} };
        file.close();
        const int32_t len = sourceText.length();
        const uint32_t shaderObject = gl.createShader(s.second);
        const char * c = sourceText.c_str();
        gl.shaderSource(shaderObject, 1, &c, &len);
        gl.compileShader(shaderObject);

        int32_t status;
        gl.getShaderiv(shaderObject, gl.COMPILE_STATUS, &status);
        if (status == gl.FALSE) {
            std::array<char, 4094> message;
            gl.getShaderInfoLog(shaderObject, message.size(), nullptr, message.data());
            message.back() = '\0';
            std::cout << "Failed to compile shader: " << s.first << "\n";
            std::cout << message.data() << std::endl;
        }
        gl.attachShader(program, shaderObject);
        gl.deleteShader(shaderObject);
        shaders.push_back(shaderObject);
    }

    gl.linkProgram(program);

    int32_t status;
    gl.getProgramiv(program, gl.LINK_STATUS, &status);
    if (status == gl.FALSE) {
        std::array<char, 4094> message;
        gl.getProgramInfoLog(program, message.size(), nullptr, message.data());
        message.back() = '\0';
        std::cout << "Failed to link program: ";
        for (size_t i = 0; i < source.size(); ++i) {
            std::cout << source[i].first;
            if (i + 1 < source.size())
                std::cout << " && ";
        }
        std::cout << std::endl;
        std::cout << message.data() << std::endl;
    }

    for (const auto & s : shaders)
        gl.detachShader(program, s);
    
    return program;
}

std::vector<int32_t> glutil::uniformLocations(
    const OpenGL & gl,
    uint32_t program,
    const std::vector<std::string> & names
) {
    std::vector<int32_t> locations;
    locations.reserve(names.size());

    for (const auto & n : names)
        locations.push_back(gl.getUniformLocation(program, n.c_str()));

    return locations;
}
