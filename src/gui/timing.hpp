#pragma once

#include <cstdint>

namespace timing {
    struct TimeSignature {
        int64_t upper = 4;
        int64_t lower = 4;
    };
}
