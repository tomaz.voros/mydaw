#version 330 core

layout (location = 0) in vec2 iPosition;

uniform vec2 uOffset;
uniform vec2 uSize;
uniform vec2 uDimensions;

void main() {
    gl_Position = vec4((iPosition * uSize + uOffset) / uDimensions * 2.0 - 1.0, 0.0, 1.0);
}
