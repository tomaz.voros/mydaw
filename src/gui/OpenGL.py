headerFileName = "OpenGL.hpp"
bodyFileName = "OpenGL.cpp"
className = "OpenGL"
indentation = "    "

# type transformations
types = {
    "GLfloat": "float",
    "GLbitfield": "uint32_t",
    "const GLchar * const *": "char const * const *",
    "const GLint *": "int32_t const *",
    "GLuint": "uint32_t",
    "GLuint *": "uint32_t *",
    "GLenum": "uint32_t",
    "GLsizei": "int32_t",
    "GLsizei *": "int32_t *",
    "void": "void",
    "const GLchar *": "char const *",
    "GLint": "int32_t",
    "GLint *": "int32_t *",
    "GLchar *": "char *",
    "const void *": "const void *",
    "GLboolean": "uint8_t",
    "GLsizeiptr": "intptr_t",
    "GLintptr": "intptr_t"
}

# defined constants
constants = [
    ("GL_DEPTH_BUFFER_BIT", "0x00000100"),
    ("GL_COLOR_BUFFER_BIT", "0x00004000"),
    ("GL_FRAGMENT_SHADER", "0x8B30"),
    ("GL_VERTEX_SHADER", "0x8B31"),
    ("GL_COMPILE_STATUS", "0x8B81"),
    ("GL_FALSE", "0"),
    ("GL_TRUE", "1"),
    ("GL_LINK_STATUS", "0x8B82"),
    ("GL_ARRAY_BUFFER", "0x8892"),
    ("GL_UNSIGNED_INT", "0x1405"),
    ("GL_STATIC_DRAW", "0x88E4"),
    ("GL_TRIANGLE_STRIP", "0x0005"),
    ("GL_NO_ERROR", "0"),
    ("GL_INT", "0x1404"),
    ("GL_UNSIGNED_BYTE", "0x1401"),
    ("GL_LINE_STRIP", "0x0003")
]

# functions according to glcorearb
functions = [
    ("glClearColor", "void", [("GLfloat", "red"), ("GLfloat", "green"), ("GLfloat", "blue"), ("GLfloat", "alpha")]),
    ("glClear", "void", [("GLbitfield", "mask")]),
    ("glGetError", "GLenum", []),
    ("glCreateProgram", "GLuint", []),
    ("glCreateShader", "GLuint", [("GLenum", "type")]),
    ("glShaderSource", "void", [("GLuint", "shader"), ("GLsizei", "count"), ("const GLchar * const *", "string"), ("const GLint *", "length")]),
    ("glCompileShader", "void", [("GLuint", "shader")]),
    ("glAttachShader", "void", [("GLuint", "program"), ("GLuint", "shader")]),
    ("glDeleteShader", "void", [("GLuint", "shader")]),
    ("glLinkProgram", "void", [("GLuint", "program")]),
    ("glDetachShader", "void", [("GLuint", "program"), ("GLuint", "shader")]),
    ("glDeleteProgram", "void", [("GLuint", "program")]),
    ("glGetUniformLocation", "GLint", [("GLuint", "program"), ("const GLchar *", "name")]),
    ("glGetShaderiv", "void", [("GLuint", "shader"), ("GLenum", "pname"), ("GLint *", "params")]),
    ("glGetShaderInfoLog", "void", [("GLuint", "shader"), ("GLsizei", "bufSize"), ("GLsizei *", "length"), ("GLchar *", "infoLog")]),
    ("glGetProgramiv", "void", [("GLuint", "program"), ("GLenum", "pname"), ("GLint *", "params")]),
    ("glGetProgramInfoLog", "void", [("GLuint", "program"), ("GLsizei", "bufSize"), ("GLsizei *", "length"), ("GLchar *", "infoLog")]),
    ("glViewport", "void", [("GLint", "x"), ("GLint", "y"), ("GLsizei", "width"), ("GLsizei", "height")]),
    ("glGenVertexArrays", "void", [("GLsizei", "n"), ("GLuint *", "arrays")]),
    ("glGenBuffers", "void", [("GLsizei", "n"), ("GLuint *", "arrays")]),
    ("glBindVertexArray", "void", [("GLuint", "array")]),
    ("glBindBuffer", "void", [("GLenum", "target"), ("GLuint", "buffer")]),
    ("glVertexAttribPointer", "void", [("GLuint", "index"), ("GLint", "size"), ("GLenum", "type"), ("GLboolean", "normalized"), ("GLsizei", "stride"), ("const void *", "pointer")]),
    ("glEnableVertexAttribArray", "void", [("GLuint", "index")]),
    ("glBufferData", "void", [("GLenum", "target"), ("GLsizeiptr", "size"), ("const void *", "data"), ("GLenum", "usage")]),
    ("glUseProgram", "void", [("GLuint", "program")]),
    ("glUniform1f", "void", [("GLint", "location"), ("GLfloat", "v0")]),
    ("glUniform2f", "void", [("GLint", "location"), ("GLfloat", "v0"), ("GLfloat", "v1")]),
    ("glUniform3f", "void", [("GLint", "location"), ("GLfloat", "v0"), ("GLfloat", "v1"), ("GLfloat", "v2")]),
    ("glUniform4f", "void", [("GLint", "location"), ("GLfloat", "v0"), ("GLfloat", "v1"), ("GLfloat", "v2"), ("GLfloat", "v3")]),
    ("glDrawArrays", "void", [("GLenum", "mode"), ("GLint", "first"), ("GLsizei", "count")]),
    ("glBufferSubData", "void", [("GLenum", "target"), ("GLintptr", "offset"), ("GLsizeiptr", "size"), ("const void *", "data")]),
    ("glUniform1i", "void", [("GLint", "location"), ("GLint", "v0")])
]

# validate
for f in functions:
    assert len(f) == 3
    assert len(f[0]) > 2
    assert f[1] in types
    for a in f[2]:
        assert len(a) == 2
        assert a[0] in types
for c in constants:
    assert len(c) == 2
    assert c[0] > 3

# generate header
header = ""
maxConstantNameLength = 0
for c in constants:
    if len(c[0]) - 3 > maxConstantNameLength:
        maxConstantNameLength = len(c[0]) - 3
maxConstantValueLength = 0
for c in constants:
    if len(c[1]) > maxConstantValueLength:
        maxConstantValueLength = len(c[1])

maxFunctionNameLength = 0
for f in functions:
    if len(f[0]) - 2 > maxFunctionNameLength:
        maxFunctionNameLength = len(f[0]) - 2

maxFunctionReturnLength = 0
for f in functions:
    if len(types[f[1]]) > maxFunctionReturnLength:
        maxFunctionReturnLength = len(types[f[1]])

maxFunctionArgsLength = 0
functionArgs = []
for f in functions:
    args = ""
    for a in f[2]:
        args += "{} {}, ".format(types[a[0]], a[1])
    if len(f[2]) > 0:
        args = args[:-2]
    if len(args) > maxFunctionArgsLength:
        maxFunctionArgsLength = len(args)
    functionArgs.append(args)

for c in constants:
    line = "{}static constexpr uint32_t {} = {};\n".format(
        indentation, c[0][3:].ljust(maxConstantNameLength, " "), c[1].rjust(maxConstantValueLength, " ")
    )
    header += line
header += "\n"
for f in functions:
    args = ""
    for a in f[2]:
        args += "{} {}, ".format(types[a[0]], a[1])
    if len(f[2]) > 0:
        args = args[:-2]
    line = "{}{} (*{}) ({}) = nullptr;\n".format(
        indentation, types[f[1]].ljust(maxFunctionReturnLength, " "), (f[0][2].lower() + f[0][3:]).ljust(maxFunctionNameLength, " "), args.ljust(maxFunctionArgsLength, " ")
    )
    header += line

header = """#pragma once

// generated file, do not edit

#include <cstdint>
#include <cstddef>

class """ + className + """ {
public:
""" + header + """
public:
""" + indentation + className + """();
""" + indentation + """~""" + className + """();
""" + indentation + className + """(""" + className + """ &&) = delete;
""" + indentation + className + """(const """ + className + """ &) = delete;
""" + indentation + className + """ & operator=(""" + className + """ &&) = delete;
""" + indentation + className + """ & operator=(const """ + className + """ &) = delete;

private:
    void * mLibGL = nullptr;

};
"""

with open(headerFileName, "w") as f:
    f.write(header)

# generate body
body = ""
for f in functions:
    var = f[0][2].lower() + f[0][3:]
    line = indentation + var + " = (decltype(" + var + "))load(\"" + f[0] + "\");\n"
    body += line

body = """// generated file, do not edit

#include \"""" + headerFileName + """\"
#include <dlfcn.h>

""" + className + """::""" + className + """() {
""" + indentation + """mLibGL = dlopen("libGL.so.1", RTLD_LAZY | RTLD_GLOBAL);
""" + indentation + """const auto getProcAddress = (void * (*) (const uint8_t *))dlsym(mLibGL, "glXGetProcAddressARB");
""" + indentation + """auto load = [handle = mLibGL, getProcAddress] (const char * name) {
""" + indentation + indentation + """return getProcAddress((const uint8_t *)name);
""" + indentation + """};

""" + body + """
}

""" + className + """::~""" + className + """() {
    dlclose(mLibGL);
}
"""

with open(bodyFileName, "w") as f:
    f.write(body)
