#version 330 core

layout (location = 0) in float iPosition;

uniform vec2 uOffset;
uniform vec2 uSize;
uniform vec2 uDimensions;
uniform int uIndexOffset;

void main() {
    vec2 p = vec2(float(gl_VertexID - uIndexOffset), iPosition);
    gl_Position = vec4((p * uSize + uOffset) / uDimensions * 2.0 - 1.0, 0.0, 1.0);
}
