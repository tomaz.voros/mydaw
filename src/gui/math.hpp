#pragma once

#include <cstdint>
#include <limits>

namespace math {
    template <typename T> struct v2 { T x = T{ 0 }, y = T{ 0 }; };
    template <typename T> struct v3 { T x = T{ 0 }, y = T{ 0 }, z = { 0 }; };
    template <typename T> struct v4 { T x = T{ 0 }, y = T{ 0 }, z = { 0 }, w = { 0 }; };
  
    using i8v2 = v2<int8_t>;
    using i16v2 = v2<int16_t>;
    using i32v2 = v2<int32_t>;
    using i64v2 = v2<int64_t>;

    using u8v2 = v2<uint8_t>;
    using u16v2 = v2<uint16_t>;
    using u32v2 = v2<uint32_t>;
    using u64v2 = v2<uint64_t>;

    using f32v2 = v2<float>;
    using f64v2 = v2<double>;

    using i8v3 = v3<int8_t>;
    using i16v3 = v3<int16_t>;
    using i32v3 = v3<int32_t>;
    using i64v3 = v3<int64_t>;

    using u8v3 = v3<uint8_t>;
    using u16v3 = v3<uint16_t>;
    using u32v3 = v3<uint32_t>;
    using u64v3 = v3<uint64_t>;

    using f32v3 = v3<float>;
    using f64v3 = v3<double>;

    using i8v4 = v4<int8_t>;
    using i16v4 = v4<int16_t>;
    using i32v4 = v4<int32_t>;
    using i64v4 = v4<int64_t>;

    using u8v4 = v4<uint8_t>;
    using u16v4 = v4<uint16_t>;
    using u32v4 = v4<uint32_t>;
    using u64v4 = v4<uint64_t>;

    using f32v4 = v4<float>;
    using f64v4 = v4<double>;

    template <typename T>
    T positiveCeilDiv(const T & a, const T & b) {
        static_assert(std::numeric_limits<T>::is_integer);
        if (a < 0) return 1;
        if (b < 1) return 1;
        return a / b + (a % b != 0);
    }

    template <typename T>
    struct ScaleTranslate {
        T sx = T{ 1 };
        T sy = T{ 1 };
        T tx = T{ 0 };
        T ty = T{ 0 };
    };

    template <typename T>
    ScaleTranslate<T> mul(const ScaleTranslate<T> & a, const ScaleTranslate<T> & b) {
        ScaleTranslate<T> c;
        c.sx = a.sx * b.sx;
        c.sy = a.sy * b.sy;
        c.tx = a.sx * b.tx + a.tx;
        c.ty = a.sy * b.ty + a.ty;
        return c;
    }
}
