#pragma once

#include <cstdint>
#include "pcm.hpp"

/**
 * \namespace inout Classes for audio input and output.
 */
namespace inout {
    /**
     * \class IInput
     * Base class for input sources.
     */
    class IInput {
    public:
        /**
         * \returns true if successfully constructed and started.
         */
        virtual bool isGood() const = 0;
        /**
         * Result is always the same for the lifetime of the object.
         * \returns Number of channels.
         */
        virtual uint64_t channelCount() const = 0;
        /**
         * Result is always the same for the lifetime of the object.
         * \returns Frame rate.
         */
        virtual uint64_t frameRate() const = 0;
        /**
         * Result is always the same for the lifetime of the object.
         * \returns Sample format.
         */
        virtual pcm::Format sampleFormat() const = 0;
        /**
         * Start input stream.
         * Check for successful start with isGood().
         */
        virtual void start() = 0;
        /**
         * Stop input stream.
         */
        virtual void stop() = 0;
        /**
         * Reads frames from input stream. If interleaved() returns true,
         * all the data is written to out[0] with channels interleaved.
         * Keep the memory allocation in mind for both cases.
         * If interleaved() returns false, each channel is written separately.
         * \param[out] out Buffer to where frames are stored.
         * \param[in] count The number of frames to be stored.
         */
        virtual void read(std::byte * * out, uint64_t count) = 0;
        /**
         * \returns true if the data returned by read() is interleaved.
         */
        virtual bool interleaved() const = 0;

        virtual ~IInput() = default;

    };

    /**
     * \class IOutput
     * Base class for output destinations.
     */
    class IOutput {
    public:
        /**
         * \returns true if successfully constructed and started.
         */
        virtual bool isGood() const = 0;
        /**
         * Result is always the same for the lifetime of the object.
         * \returns Number of channels.
         */
        virtual uint64_t channelCount() const = 0;
        /**
         * Result is always the same for the lifetime of the object.
         * \returns Frame rate.
         */
        virtual uint64_t frameRate() const = 0;
        /**
         * Result is always the same for the lifetime of the object.
         * \returns Sample format.
         */
        virtual pcm::Format sampleFormat() const = 0;
        /**
         * Start output stream.
         * Check for successful start with isGood().
         */
        virtual void start() = 0;
        /**
         * Stop output stream.
         */
        virtual void stop() = 0;
        /**
         * Block until all passed in frames have been played.
         */
        virtual void wait() = 0;
        /**
         * \returns true if the data to be sent to write() must be interleaved.
         */
        virtual bool interleaved() const = 0;
        /**
         * Write frames to output stream. If interleaved() returns true,
         * all the data is read from in[0] with channels interleaved.
         * If interleaved() returns false, each channel is read from separately.
         * The function might not wait until all data has been played back.
         * \param[in] in Buffer to frames that will be read.
         * \param[in] count Number of frames in buffer.
         */
        virtual void write(const std::byte * * in, uint64_t count) = 0;

        virtual ~IOutput() = default;

    };

    /**
     * \class OutputPA Implements output to PulseAudio simple api.
     */
    class OutputPA : public IOutput {
    public:
        OutputPA(
            uint64_t frameRate,
            uint64_t channelCount,
            const uint64_t * channelIndices,
            pcm::Format sampleFormat
        );//
        ~OutputPA() override;
        bool isGood() const override;
        uint64_t channelCount() const override;
        uint64_t frameRate() const override;
        pcm::Format sampleFormat() const override;
        void start() override;
        void stop() override;
        void wait() override;
        bool interleaved() const override;
        void write(const std::byte * * in, uint64_t count) override;

    private:
        uint64_t mFrameRate = 0;
        uint64_t mChannelCount = 0;
        pcm::Format mSampleFormat = pcm::Format::UNKNOWN;
        uint64_t mSampleSize = 0;
        std::aligned_storage<152>::type mImpl;

    };

    /**
     * \class InputPA Implements input to PulseAudio simple api.
     */
    class InputPA : public IInput {
    public:
        InputPA(
            uint64_t frameRate,
            uint64_t channelCount,
            const uint64_t * channelIndices,
            pcm::Format sampleFormat
        );
        ~InputPA() override;
        bool isGood() const override;
        uint64_t channelCount() const override;
        uint64_t frameRate() const override;
        pcm::Format sampleFormat() const override;
        void start() override;
        void stop() override;
        void read(std::byte * * out, uint64_t count) override;
        bool interleaved() const override;

    private:
        uint64_t mFrameRate = 0;
        uint64_t mChannelCount = 0;
        pcm::Format mSampleFormat = pcm::Format::UNKNOWN;
        uint64_t mSampleSize = 0;
        std::aligned_storage<152>::type mImpl;

    };

}
