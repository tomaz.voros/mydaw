#pragma once

#include <set>
#include <memory>
#include "pcm.hpp"
#include "mem.hpp"

/**
 * \namespace mix Clip mixing utilities.
 */
namespace mix {
    struct ClipPlacement {
        std::shared_ptr<pcm::ClipStream> clip;
        uint64_t position;
        uint64_t length;
        uint64_t offset;
    };

    // C++ why?
    /**
     * Used to sort ClipPlacement.
     */
    struct ClipPlacementComparators {
        using is_transparent = void;
        bool operator()(const ClipPlacement & a, const ClipPlacement & b) const;
        bool operator()(const ClipPlacement & a, uint64_t b) const;
    };

    class Track {
    public:
        Track(uint64_t channelCount, uint64_t frameRate);
        /**
         * Reads count frames in to out and advances the head by count.
         * \param[out] out Buffer to which the frames are stored.
         * \param[in] count Number of frames to be read.
         * \param buff Buffers used internally for reading clips. Buffer
         *             must contain max buffer size returned by any of the Clip::bufferSizeRequirement()
         *             from any clip successfully passed with addClip().
         *             Number of channels must be at least channelCount().
         *             mem::Buffer::channelDummies must be an array to float * of size returned by channelCount().
         */
        void read(float * * out, uint64_t count, const mem::Buffer & buff);
        /**
         * Sets head to position.
         * \param[in] position Position of the next frame to be extracted by read().
         */
        void seek(uint64_t position);
        /**
         * Adds clip clip to track. The ownership is transferred.
         * \param[in] clip Clip to be placed at given position on the track.
         * \returns true if clip placed successfully. false if there was no space.
         */
        bool addClip(ClipPlacement && clipPlacement);
        /**
         * \returns the number of channels in the track.
         */
        uint64_t channelCount() const;
        /**
         * \param[in] frameCount For this number of frames.
         * \returns Amounts needed for allocation. Buffers are not initialized.
         */
        mem::Buffer memoryRequirement(uint64_t frameCount) const;

    private:
        std::set<ClipPlacement, ClipPlacementComparators> mClipPlacements;
        decltype(mClipPlacements)::iterator mCurrentClip;
        uint64_t mChannelCount = 0;
        uint64_t mFrameRate = 0;
        uint64_t mHead = 0;

        /**
         * Last FADE_OUT_LENGTH samples are faded out.
         */
        static constexpr int64_t FADE_OUT_LENGTH = 100; // TODO: configure per ClipPlacement and move to a gain system
        static constexpr int64_t FADE_IN_LENGTH = 100; // TODO: configure per ClipPlacement and move to a gain system
        /**
         * Enable (true) or disable (false) fade out at the end of the clip or in ad the beginning of the clip
         */
        static constexpr bool FADE_OUT = true;
        static constexpr bool FADE_IN = true;

    };

}
