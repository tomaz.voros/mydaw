#include "mix.hpp"
#include <algorithm>
#include <cassert>

mix::Track::Track(uint64_t channelCount, uint64_t frameRate) :
    mChannelCount{ channelCount },
    mFrameRate{ frameRate },
    mCurrentClip{ mClipPlacements.end() }
{}

uint64_t mix::Track::channelCount() const {
    return mChannelCount;
}

mem::Buffer mix::Track::memoryRequirement(uint64_t frameCount) const {
    mem::Buffer req;
    req.channelCount = mChannelCount;
    for (const auto & c : mClipPlacements) {
        const auto r = c.clip->bufferSizeRequirement(frameCount);
        req.frameCount = std::max(req.frameCount, r.second);
        req.byteCount = std::max(req.byteCount, r.first);
    }
    return req;
}

void mix::Track::read(float * * out, uint64_t count, const mem::Buffer & buff) {
    auto & i = mCurrentClip;
    uint64_t outHead = 0;
    while (i != mClipPlacements.end() && count > 0) {
        if (mHead < i->position) {
            const uint64_t preSilence = std::min(i->position - mHead, count);
            for (uint64_t c = 0; c < mChannelCount; ++c)
                std::fill(out[c] + outHead, out[c] + outHead + preSilence, float{ 0 });
            count -= preSilence;
            outHead += preSilence;
            mHead += preSilence;
        // TODO: check for off by on in comparing with '<'
        } else if (const uint64_t inClipPosition = mHead - i->position; inClipPosition < i->length) {
            const uint64_t clipLen = std::min(i->length - inClipPosition, count);

            for (uint64_t c = 0; c < i->clip->channelCount(); ++c)
                buff.channelDummies[c] = out[c] + outHead;
            i->clip->read(buff.channelDummies, inClipPosition + i->offset, clipLen, buff.bytes, buff.channels);
            for (uint64_t c = i->clip->channelCount(); c < mChannelCount; ++c)
                std::fill(out[c] + outHead, out[c] + outHead + clipLen, float{ 0 });

            if (FADE_OUT) {
                // TODO: configure fade starts and fade ends per ClipPlacement (maybe do this in a separate system from Track)
                const int64_t fadeOutEnd = static_cast<int64_t>(i->length);
                const int64_t fadeOutStart = std::max<int64_t>(fadeOutEnd - FADE_OUT_LENGTH, 0);
                const int64_t currentStart = inClipPosition + i->offset;
                const int64_t currentEnd = currentStart + int64_t(clipLen);
                const int64_t fadeOutFrom = std::max(fadeOutStart, currentStart);
                const int64_t fadeOutTo = std::min(fadeOutEnd, currentEnd);
                const int64_t fadeOffset = std::max<int64_t>(fadeOutFrom - currentStart, 0);

                for (uint64_t c = 0; c < i->clip->channelCount(); ++c) {
                    for (int64_t s = fadeOutFrom; s < fadeOutTo; ++s) {
                        // first sample is not attenuated
                        const float a = 1.0f - float(s - fadeOutStart) / float(FADE_OUT_LENGTH);
                        assert(a >= 0.0f && a <= 1.0f); // hmm
                        buff.channelDummies[c][s - currentStart] *= a;
                    }
                }
                // no need to fade out silence (rest of track channels)
            }

            if (FADE_IN) {
                const int64_t fadeInPosition = std::min<int64_t>(inClipPosition, FADE_IN_LENGTH);
                const int64_t fadeInLeft = FADE_IN_LENGTH - fadeInPosition;
                const int64_t fadeInThisClip = std::min<int64_t>(fadeInLeft, clipLen);
                
                for (uint64_t c = 0; c < i->clip->channelCount(); ++c) {
                    for (int64_t s = 0; s < fadeInThisClip; ++s) {
                        const int64_t fadeInIndex = fadeInPosition + s;
                        const float a = float(fadeInIndex) / float(FADE_IN_LENGTH);
                        assert(a >= 0.0f && a <= 1.0f);
                        const int64_t sampleInBufferIndex = fadeInIndex - fadeInPosition;
                        assert(sampleInBufferIndex >= 0 && sampleInBufferIndex < clipLen);
                        buff.channelDummies[c][sampleInBufferIndex] *= a;
                    }
                }
            }

            count -= clipLen;
            outHead += clipLen;
            mHead += clipLen;
        } else {
            ++i;
        }
    }

    if (i == mClipPlacements.end()) {
        for (uint64_t c = 0; c < mChannelCount; ++c)
            std::fill(out[c] + outHead, out[c] + outHead + count, float{ 0 });
        mHead += count;
    }
}

void mix::Track::seek(uint64_t position) {
    mHead = position;
    auto it = mClipPlacements.lower_bound<decltype(position)>(position);
    if (it != mClipPlacements.begin())
        --it;
    mCurrentClip = it;
}

bool mix::ClipPlacementComparators::operator()(const ClipPlacement & a, const ClipPlacement & b) const {
    return a.position < b.position;
}

bool mix::ClipPlacementComparators::operator()(const ClipPlacement & a, uint64_t b) const {
    return a.position < b;
}

bool mix::Track::addClip(ClipPlacement && clipPlacement) {
    if (!clipPlacement.clip->isGood())
        return false;
    if (!clipPlacement.clip->channelCount() > mChannelCount)
        return false;
    if (clipPlacement.length == 0)
        return false;

    uint64_t startSpace = 0;
    uint64_t endSpace = std::numeric_limits<uint64_t>::max();

    auto it = mClipPlacements.upper_bound(clipPlacement);

    if (it != mClipPlacements.end())
        endSpace = it->position;
    if (it != mClipPlacements.begin()) {
        --it;
        startSpace = it->position + it->length;
    }

    if (
        clipPlacement.position >= startSpace
        &&
        clipPlacement.position + clipPlacement.length <= endSpace
    ) {
        mClipPlacements.insert(std::move(clipPlacement));
        seek(mHead);
        return true;
    } else {
        return false;
    }
}
