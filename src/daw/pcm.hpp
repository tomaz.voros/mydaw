#pragma once

#include <fstream>
#include <cstdint>
#include <cstddef>
#include <filesystem.hpp>
#include <utility>

/**
 * \namespace pcm Utilities for working with PCM sample data.
 */
namespace pcm {
    static_assert(std::numeric_limits<float>::is_iec559);

    /**
     * When changing this enum, also update sampleSize().
     */
    enum class Format {
        UNKNOWN,

        // LE
        INT8_LE,
        INT16_LE,
        INT24_LE,
        INT32_LE,

        UINT8_LE,
        UINT16_LE,
        UINT24_LE,
        UINT32_LE,

        FLOAT32_LE,

        // BE
        INT8_BE,
        INT16_BE,
        INT24_BE,
        INT32_BE,

        UINT8_BE,
        UINT16_BE,
        UINT24_BE,
        UINT32_BE,

        FLOAT32_BE
    };

    namespace wav {
        /**
         * Stores values to wave file.
         * \param fileName Path to the file to which to store the data.
         * \param clip Vector (clip) of Vectors (channels) of samples. All channels must have the same length.
         * \param frameRate Frame rate of the clip.
         */
        void save(
            const std::filesystem::path & fileName,
            const std::vector<std::vector<float>> & clip,
            uint32_t frameRate
        );
    }

    namespace resampling {
        class IResampler {
        public:
            /**
             * \returns true if resampler was initialized properly.
             */
            virtual bool isGood() const = 0;
            /**
             * Resamples values from in to out based on last call to nextRange().
             * The result is meaningless if the last call to isGood() returned false.
             */
            virtual void resample(float * in, float * out) = 0;
            /**
             * \returns input samples offset (result.first) and input sample count (result.second) needed
             * for output range outStart to outStart+outSize.
             * Sets that range to be processed by the next call to resample.
             * The result is meaningless if the last call to isGood() returned false.
             */
            virtual std::pair<uint64_t, uint64_t> nextRange(uint64_t outStart, uint64_t outSize) = 0;
            /**
             * \returns Maximum possible input size required for given output size.
             * Useful when allocating memory for input. The result is meaningless if
             * last call to isGood() returned false.
             */
            virtual uint64_t maxRangeSize(uint64_t outSize) const = 0;

            virtual ~IResampler() = default;
        };

        class Nearest : public IResampler {
        public:
            Nearest() = default;
            Nearest(uint64_t inRate, uint64_t outRate);
            bool isGood() const override;
            void resample(float * in, float * out) override;
            std::pair<uint64_t, uint64_t> nextRange(uint64_t outStart, uint64_t outSize) override;
            uint64_t maxRangeSize(uint64_t outSize) const override;

        private:
            uint64_t mInRate = 0;
            uint64_t mOutRate = 0;
            uint64_t mInStart = 0;
            uint64_t mOutStart = 0;
            uint64_t mOutSize = 0;

        };

        class Linear : public IResampler {
        public:
            Linear() = default;
            Linear(uint64_t inRate, uint64_t outRate);
            bool isGood() const override;
            void resample(float * in, float * out) override;
            std::pair<uint64_t, uint64_t> nextRange(uint64_t outStart, uint64_t outSize) override;
            uint64_t maxRangeSize(uint64_t outSize) const override;

        private:
            uint64_t mInRate = 0;
            uint64_t mOutRate = 0;
            uint64_t mInStart = 0;
            uint64_t mOutStart = 0;
            uint64_t mOutSize = 0;

        };

    }

    namespace constant {
        constexpr int16_t POW_2_07_I = 128;
        constexpr int32_t POW_2_15_I = 32768;
        constexpr int32_t POW_2_23_I = 8388608;
        constexpr int64_t POW_2_31_I = 2147483648;

        constexpr float POW_2_07 = POW_2_07_I;
        constexpr float POW_2_15 = POW_2_15_I;
        constexpr float POW_2_23 = POW_2_23_I;
        constexpr float POW_2_31 = POW_2_31_I;

        constexpr float INV_POW_2_07 = float(1) / POW_2_07;
        constexpr float INV_POW_2_15 = float(1) / POW_2_15;
        constexpr float INV_POW_2_23 = float(1) / POW_2_23;
        constexpr float INV_POW_2_31 = float(1) / POW_2_31;
    }

    //@{
    /**
     * Converts sampleCount samples in data from format format to internal processing format and stores them in result.
     * Note that sampleCount = frameCount * samplesPerFrame.
     * \param[in] data Input sample data.
     * \param[in] format Input sample format.
     * \param[in] sampleCount Number of samples to be processed.
     * \param[out] result Destination for the converted samples.
     * Deinterleaving versions store each channel in a separate result array.
     */
    void castAnySamplesToFloatNE(const std::byte * data, Format format, uint64_t sampleCount, float * result);
    void deinterleaveCastAnySamplesToFloatNE(const std::byte * data, Format format, uint64_t sampleCount, uint64_t channelCount, float * * result);
    template <Format format> void deinterleaveCastSamplesToFloatNE(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result);
    template <Format format> void castSamplesToFloatNE(const std::byte * data, uint64_t sampleCount, float * result) {
        deinterleaveCastSamplesToFloatNE<format>(data, sampleCount, 1, &result);
    }
    //@}

    /**
     * \param[in] format Format of which the size is to be determined.
     * \returns Sample size in bytes.
     */
    uint64_t sampleSize(Format format);

    /**
     * Information about interleaved PCM data.
     */
    struct Meta {
        Format format = Format::UNKNOWN;
        uint64_t frameRate = 0;
        uint64_t frameCount = 0;
        uint64_t channelCount = 0;
    };

    /**
     * Custom move assignment and move constructors are used.
     * Update them accordingly when changing something.
     */
    class Clip {
    public:
        Clip() = default;
        ~Clip() = default;

        Clip(const std::filesystem::path & path);
        Clip & operator=(Clip && other);
        Clip(Clip && other);

        Clip & operator=(const Clip & other) = delete;
        Clip(const Clip & other) = delete;

        /**
         * \returns File path of the clip.
         */
        const std::filesystem::path & getPath() const;
        
        /**
         * \returns true if file is open and successfully parsed.
         */
        bool isGood() const;
        
        /**
         * If the last call to isGood() returned false, the result of this
         * function is meaningless.
         * \returns Meta information about the clip.
         */
        Meta getMeta() const;

        /**
         * \returns Number of read frames.
         * \param[out] data Array to where the samples are stored.
         * \param[in] frameOffset Frame position of the first frame to be read.
         * \param[in] frameCount Number of frames to be read.
         */
        uint64_t read(std::byte * data, uint64_t frameOffset, uint64_t frameCount);

        /**
         * \returns Number of bytes needed per frame.
         */
        uint64_t frameSize() const;

    private:
        std::filesystem::path mPath;
        std::ifstream mFile;
        Meta mMeta;
        uint64_t mDataStart = 0;
        uint64_t mFrameSize = 0;

        bool parseWAVE();
        bool parseAIFF();
        bool parseAIFC();

    };

    /**
     * \class ClipStream
     * Used as an abstraction of pcm::Clip. Converts the file data samples
     * to requested frame rate in float format.
     */
    class ClipStream {
    public:
        /**
         * \param[in] clipPath Path to a clip file.
         * \param[in] outRate Output frame rate.
         */
        ClipStream(const std::filesystem::path & clipPath, uint64_t outRate);
        /**
         * Reads \p size frames to \p out. Deinterleaves, converts and resamples input data to float format.
         * Requested data should avoid going past the end of the clip. Past the clip bounds, values of 0 are assumed.
         * \param[out] out Array of channels to where the result is written.
         * \param[in] position Starting frame position.
         * \param[in] size Number of frames to be returned.
         * \param byteBuffer Buffer used internally. Must be able to hold the size returned by bufferSizeRequirement().
         * \param floatBuffer Buffers used internally. Must be able to hold the size returned by bufferSizeRequirement() for each channel.
         */
        // TODO: take mem::Buffer instead and check (assert) parameters
        void read(float * * out, uint64_t position, uint64_t size, std::byte * byteBuffer, float * * floatBuffer);
        /**
         * \returns true if the clip is open and valid.
         */
        bool isGood() const;
        /**
         * \returns Number of channels.
         */
        uint64_t channelCount() const;
        /**
         * \param[in] frameCount Number of frames that might be requested in call to read().
         * \returns Needed byteBuffer size (first) and floatBuffer size for each channel (second) when calling read().
         */
        std::pair<uint64_t, uint64_t> bufferSizeRequirement(uint64_t frameCount) const;
        /**
         * \returns File path of the clip.
         */
        const std::filesystem::path & clipPath() const;
        /**
         * \returns Meta data of the source clip.
         */
        Meta getClipMeta() const;

        // TODO: implement ability to mutate / change outRate (for geneerating visualization)

        // TODO: maybe explicit prefetching

    private:
        Clip mClip;
        resampling::Linear mResampler;
        Format mClipFormat = Format::UNKNOWN;
        uint64_t mClipFormatByteSize = 0;
        uint64_t mChannelCount = 0;

    };

}
