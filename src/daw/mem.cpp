#include "mem.hpp"

void mem::allocateBuffer(mem::Buffer & buffer) {
    buffer.channels = new float * [buffer.channelCount];
    for (size_t c = 0; c < buffer.channelCount; ++c)
        buffer.channels[c] = new float[buffer.frameCount];
    buffer.channelDummies = new float * [buffer.channelCount];
    buffer.bytes = new std::byte [buffer.byteCount];
}

void mem::deallocateBuffer(mem::Buffer & buffer) {
    for (size_t c = 0; c < buffer.channelCount; ++c)
        delete[] buffer.channels[c];
    delete[] buffer.channels;
    delete[] buffer.channelDummies;
    delete[] buffer.bytes;
}
