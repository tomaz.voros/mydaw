#include "inout.hpp"

#include <stdexcept>
#include <vector>
#include <pulse/simple.h>

namespace {
    struct InputPAImpl {
        pa_simple * server = nullptr;
        pa_channel_map map = {};
        pa_sample_spec spec = {};
    };
    struct OutputPAImpl {
        pa_simple * server = nullptr;
        pa_channel_map map = {};
        pa_sample_spec spec = {};
    };
}

inout::OutputPA::OutputPA(
    uint64_t frameRate,
    uint64_t channelCount,
    const uint64_t * channelIndices,
    pcm::Format sampleFormat
) {    
    static_assert(sizeof(mImpl) >= sizeof(OutputPAImpl), "Please fix the hard coded constant in mImpl declaration.");
    new (static_cast<void *>(std::addressof(mImpl))) OutputPAImpl;

    // parameter check
    if (frameRate < 1 || frameRate > PA_RATE_MAX) return;
    if (channelCount < 1 || channelCount > PA_CHANNELS_MAX) return;
    if (sampleFormat == pcm::Format::UNKNOWN) return;
    pa_sample_format internalSampleFormat = PA_SAMPLE_INVALID;
    switch (sampleFormat) {
        case pcm::Format::UINT8_LE:   internalSampleFormat = PA_SAMPLE_U8;        break;
        case pcm::Format::INT16_LE:   internalSampleFormat = PA_SAMPLE_S16LE;     break;
        case pcm::Format::INT24_LE:   internalSampleFormat = PA_SAMPLE_S24LE;     break;
        case pcm::Format::INT32_LE:   internalSampleFormat = PA_SAMPLE_S32LE;     break;
        case pcm::Format::FLOAT32_LE: internalSampleFormat = PA_SAMPLE_FLOAT32LE; break;
        case pcm::Format::UINT8_BE:   internalSampleFormat = PA_SAMPLE_U8;        break;
        case pcm::Format::INT16_BE:   internalSampleFormat = PA_SAMPLE_S16BE;     break;
        case pcm::Format::INT24_BE:   internalSampleFormat = PA_SAMPLE_S24BE;     break;
        case pcm::Format::INT32_BE:   internalSampleFormat = PA_SAMPLE_S32BE;     break;
        case pcm::Format::FLOAT32_BE: internalSampleFormat = PA_SAMPLE_FLOAT32BE; break;
        // TODO: support for PA_SAMPLE_S24_32LE and PA_SAMPLE_S24_32BE
    }
    for (uint64_t c = 0; c < channelCount; ++c) {
        if (channelIndices[c] < 0 || channelIndices[c] >= PA_CHANNEL_POSITION_MAX)
            return;
    }

    mFrameRate = frameRate;
    mChannelCount = channelCount;
    mSampleFormat = sampleFormat;
    mSampleSize = pcm::sampleSize(sampleFormat);

    auto impl = std::launder(reinterpret_cast<OutputPAImpl *>(std::addressof(mImpl)));

    impl->server = nullptr;

    impl->spec.format = internalSampleFormat;
    impl->spec.rate = mFrameRate;
    impl->spec.channels = mChannelCount;

    impl->map.channels = channelCount;
    for (uint64_t c = 0; c < channelCount; ++c) {
        impl->map.map[c] = static_cast<pa_channel_position>(channelIndices[c]);
    }
}

inout::InputPA::InputPA(
    uint64_t frameRate,
    uint64_t channelCount,
    const uint64_t * channelIndices,
    pcm::Format sampleFormat
) {
    static_assert(sizeof(mImpl) >= sizeof(InputPAImpl), "Please fix the hard coded constant in mImpl declaration.");
    new (static_cast<void *>(std::addressof(mImpl))) InputPAImpl;

    // parameter check
    if (frameRate < 1 || frameRate > PA_RATE_MAX) return;
    if (channelCount < 1 || channelCount > PA_CHANNELS_MAX) return;
    if (sampleFormat == pcm::Format::UNKNOWN) return;
    pa_sample_format internalSampleFormat = PA_SAMPLE_INVALID;
    switch (sampleFormat) {
        case pcm::Format::UINT8_LE:   internalSampleFormat = PA_SAMPLE_U8;        break;
        case pcm::Format::INT16_LE:   internalSampleFormat = PA_SAMPLE_S16LE;     break;
        case pcm::Format::INT24_LE:   internalSampleFormat = PA_SAMPLE_S24LE;     break;
        case pcm::Format::INT32_LE:   internalSampleFormat = PA_SAMPLE_S32LE;     break;
        case pcm::Format::FLOAT32_LE: internalSampleFormat = PA_SAMPLE_FLOAT32LE; break;
        case pcm::Format::UINT8_BE:   internalSampleFormat = PA_SAMPLE_U8;        break;
        case pcm::Format::INT16_BE:   internalSampleFormat = PA_SAMPLE_S16BE;     break;
        case pcm::Format::INT24_BE:   internalSampleFormat = PA_SAMPLE_S24BE;     break;
        case pcm::Format::INT32_BE:   internalSampleFormat = PA_SAMPLE_S32BE;     break;
        case pcm::Format::FLOAT32_BE: internalSampleFormat = PA_SAMPLE_FLOAT32BE; break;
        // TODO: support for PA_SAMPLE_S24_32LE and PA_SAMPLE_S24_32BE
    }
    for (uint64_t c = 0; c < channelCount; ++c) {
        if (channelIndices[c] < 0 || channelIndices[c] >= PA_CHANNEL_POSITION_MAX)
            return;
    }

    mFrameRate = frameRate;
    mChannelCount = channelCount;
    mSampleFormat = sampleFormat;
    mSampleSize = pcm::sampleSize(sampleFormat);

    auto impl = std::launder(reinterpret_cast<InputPAImpl *>(std::addressof(mImpl)));

    impl->server = nullptr;

    impl->spec.format = internalSampleFormat;
    impl->spec.rate = mFrameRate;
    impl->spec.channels = mChannelCount;

    impl->map.channels = channelCount;
    for (uint64_t c = 0; c < channelCount; ++c) {
        impl->map.map[c] = static_cast<pa_channel_position>(channelIndices[c]);
    }
}

bool inout::InputPA::isGood() const {
    const auto impl = std::launder(reinterpret_cast<const InputPAImpl *>(std::addressof(mImpl)));
    return impl->server != nullptr;
}

bool inout::OutputPA::isGood() const {
    const auto impl = std::launder(reinterpret_cast<const OutputPAImpl *>(std::addressof(mImpl)));
    return impl->server != nullptr;
}

uint64_t inout::InputPA::channelCount() const {
    return mChannelCount;
}

uint64_t inout::OutputPA::channelCount() const {
    return mChannelCount;
}

uint64_t inout::InputPA::frameRate() const {
    return mFrameRate;
}

pcm::Format inout::InputPA::sampleFormat() const {
    return mSampleFormat;
}

uint64_t inout::OutputPA::frameRate() const {
    return mFrameRate;
}

pcm::Format inout::OutputPA::sampleFormat() const {
    return mSampleFormat;
}

void inout::InputPA::start() {
    auto impl = std::launder(reinterpret_cast<InputPAImpl *>(std::addressof(mImpl)));
    if (impl->server != nullptr) {
        const int flushResult = pa_simple_flush(impl->server, nullptr);
        if (flushResult != 0) {
            stop();
        }
    }

    impl->server = pa_simple_new(
        nullptr,
        "myDaw",
        PA_STREAM_RECORD,
        nullptr,
        "in",
        &impl->spec,
        &impl->map,
        nullptr,
        nullptr
    );

    if (impl->server == nullptr)
        return;

    const auto flushResult = pa_simple_flush(impl->server, nullptr);
    if (flushResult != 0)
        stop();
}

void inout::OutputPA::start() {
    auto impl = std::launder(reinterpret_cast<OutputPAImpl *>(std::addressof(mImpl)));
    if (impl->server != nullptr) {
        stop();
    }

    impl->server = pa_simple_new(
        nullptr,
        "myDaw",
        PA_STREAM_PLAYBACK,
        nullptr,
        "out",
        &impl->spec,
        &impl->map,
        nullptr,
        nullptr
    );

    if (impl->server == nullptr)
        return;

    const auto flushResult = pa_simple_flush(impl->server, nullptr);
    if (flushResult != 0)
        stop();
}

void inout::InputPA::read(std::byte * * out, uint64_t count) {
    const auto impl = std::launder(reinterpret_cast<InputPAImpl *>(std::addressof(mImpl)));

    bool success = false;

    if (impl->server != nullptr) {
        const auto readResult = pa_simple_read(
            impl->server,
            static_cast<void *>(out[0]),
            count * mChannelCount * mSampleSize,
            nullptr
        );
        if (readResult == 0)
            success = true;
    }

    // set silence
    if (!success)
        // TODO: use correct type to fill!
        std::fill(out[0], out[0] + count * mChannelCount * mSampleSize, std::byte{ 0 });
}

void inout::OutputPA::write(const std::byte * * in, uint64_t count) {
    const auto impl = std::launder(reinterpret_cast<OutputPAImpl *>(std::addressof(mImpl)));

    bool success = false;

    if (impl->server != nullptr) {
        const auto readResult = pa_simple_write(
            impl->server,
            static_cast<const void *>(in[0]),
            count * mChannelCount * mSampleSize,
            nullptr
        );
        if (readResult == 0)
            success = true;
    }

    if (!success)
        stop();
}

void inout::InputPA::stop() {
    auto impl = std::launder(reinterpret_cast<InputPAImpl *>(std::addressof(mImpl)));
    if (impl->server != nullptr) {
        pa_simple_free(impl->server);
        impl->server = nullptr;
    }
}

void inout::OutputPA::stop() {
    auto impl = std::launder(reinterpret_cast<OutputPAImpl *>(std::addressof(mImpl)));
    if (impl->server != nullptr) {
        pa_simple_free(impl->server);
        impl->server = nullptr;
    }
}

void inout::OutputPA::wait() {
    auto impl = std::launder(reinterpret_cast<OutputPAImpl *>(std::addressof(mImpl)));
    if (impl->server != nullptr) {
        const auto drainResult = pa_simple_drain(impl->server, nullptr);
        if (drainResult != 0)
            stop();
    }
}

inout::InputPA::~InputPA() {
    stop();
    auto impl = std::launder(reinterpret_cast<InputPAImpl *>(std::addressof(mImpl)));
    impl->~InputPAImpl();
}

inout::OutputPA::~OutputPA() {
    stop();
    auto impl = std::launder(reinterpret_cast<OutputPAImpl *>(std::addressof(mImpl)));
    impl->~OutputPAImpl();
}

bool inout::InputPA::interleaved() const {
    return true;
}

bool inout::OutputPA::interleaved() const {
    return true;
}
