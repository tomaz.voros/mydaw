#pragma once

#include <cstddef>

/**
 * \namespace mem Buffer allocation helper.
 */
namespace mem {
    struct Buffer {
        // TODO: RAII
        float * * channels = nullptr;
        float * * channelDummies = nullptr;
        std::byte * bytes = nullptr;
        size_t channelCount = 0;
        size_t frameCount = 0;
        size_t byteCount = 0;
    };

    /**
     * Not necessary to use this function for allocation.
     */
    void allocateBuffer(mem::Buffer & buffer);
    /**
     * Not necessary to use this function for allocation.
     */
    void deallocateBuffer(mem::Buffer & buffer);

    /**
     * Interleaves aka. transposes the data from in to out.
     * \param[in] in De-interleaved array to be interleaved.
     *               The array must point to channelCount pointers.
     *               Each pointer in the array must point to frameCount
     *               elements.
     * \param[out] out Location to where the interleaved data is stored.
     *                 The pointer must point to an array of
     *                 frameCount * channelCount elements.
     * \param[in] channelCount Number of channels.
     * \param[in] frameCount Number of frames.
     */
    template <typename T>
    void interleave(T * * in, T * out, size_t channelCount, size_t frameCount) {
        for (size_t f = 0; f < frameCount; ++f)
            for (size_t c = 0; c < channelCount; ++c)
                *out++ = in[c][f];
    }
}
