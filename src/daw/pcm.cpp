#include "pcm.hpp"
#include <array>
#include <climits>
#include "type_traits.hpp"

template <pcm::Format format> void pcm::deinterleaveCastSamplesToFloatNE(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    throw std::runtime_error("Not implemented.");
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::INT8_LE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            const int8_t u = static_cast<int8_t>(*data);
            result[channel][sample] = static_cast<float>(u) * constant::INV_POW_2_07;
            data += 1;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::INT16_LE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            const int16_t u = static_cast<int16_t>(data[0]) | static_cast<int16_t>(data[1]) << static_cast<int16_t>(8);
            result[channel][sample] = static_cast<float>(u) * constant::INV_POW_2_15;
            data += 2;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::INT24_LE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            const int32_t a = static_cast<int32_t>(data[2]) << static_cast<int32_t>(24) >> static_cast<int32_t>(8);
            const int32_t b = static_cast<int32_t>(data[1]) << static_cast<int32_t>(8);
            const int32_t c = static_cast<int32_t>(data[0]);
            const int32_t u = a | b | c;
            result[channel][sample] = static_cast<float>(u) * constant::INV_POW_2_23;
            data += 3;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::INT32_LE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            const int32_t a = static_cast<int32_t>(data[3]) << static_cast<int32_t>(24);
            const int32_t b = static_cast<int32_t>(data[2]) << static_cast<int32_t>(16);
            const int32_t c = static_cast<int32_t>(data[1]) << static_cast<int32_t>(8);
            const int32_t d = static_cast<int32_t>(data[0]);
            const int32_t u = a | b | c | d;
            result[channel][sample] = static_cast<float>(u) * constant::INV_POW_2_31;
            data += 4;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::UINT8_LE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            const int8_t u = static_cast<int8_t>(static_cast<int16_t>(*data) - constant::POW_2_07_I);
            result[channel][sample] = static_cast<float>(u) * constant::INV_POW_2_07;
            data += 1;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::UINT16_LE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            const uint16_t u = static_cast<uint16_t>(data[0]) | static_cast<uint16_t>(data[1]) << static_cast<uint16_t>(8);
            const int16_t v = static_cast<int16_t>(static_cast<int32_t>(u) - constant::POW_2_15_I);
            result[channel][sample] = static_cast<float>(v) * constant::INV_POW_2_15;
            data += 2;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::UINT24_LE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            const uint32_t a = static_cast<uint32_t>(data[2]) << static_cast<uint32_t>(16);
            const uint32_t b = static_cast<uint32_t>(data[1]) << static_cast<uint32_t>(8);
            const uint32_t c = static_cast<uint32_t>(data[0]);
            const uint32_t u = a | b | c;
            const int32_t v = static_cast<int32_t>(u) - constant::POW_2_23_I;
            result[channel][sample] = static_cast<float>(v) * constant::INV_POW_2_23;
            data += 3;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::UINT32_LE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            const uint32_t a = static_cast<uint32_t>(data[3]) << static_cast<uint32_t>(24);
            const uint32_t b = static_cast<uint32_t>(data[2]) << static_cast<uint32_t>(16);
            const uint32_t c = static_cast<uint32_t>(data[1]) << static_cast<uint32_t>(8);
            const uint32_t d = static_cast<uint32_t>(data[0]);
            const uint32_t u = a | b | c | d;
            const int32_t v = static_cast<int32_t>(static_cast<int64_t>(u) - constant::POW_2_31_I);
            result[channel][sample] = static_cast<float>(v) * constant::INV_POW_2_31;
            data += 4;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::FLOAT32_LE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            if constexpr (std::endian::native == std::endian::little)
                for (size_t byte = 0; byte < 4; ++byte)
                    reinterpret_cast<std::byte *>(result[channel] + sample)[byte] = data[byte];
            else
                for (size_t byte = 0; byte < 4; ++byte)
                    reinterpret_cast<std::byte *>(result[channel] + sample)[byte] = data[3 - byte];
            data += 4;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::INT8_BE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            const int8_t u = static_cast<int8_t>(*data);
            result[channel][sample] = static_cast<float>(u) * constant::INV_POW_2_07;
            data += 1;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::INT16_BE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            const int16_t u = static_cast<int16_t>(data[0]) << static_cast<int16_t>(8) | static_cast<int16_t>(data[1]);
            result[channel][sample] = static_cast<float>(u) * constant::INV_POW_2_15;
            data += 2;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::INT24_BE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            const int32_t a = static_cast<int32_t>(data[0]) << static_cast<int32_t>(24) >> static_cast<int32_t>(8);
            const int32_t b = static_cast<int32_t>(data[1]) << static_cast<int32_t>(8);
            const int32_t c = static_cast<int32_t>(data[2]);
            const int32_t u = a | b | c;
            result[channel][sample] = static_cast<float>(u) * constant::INV_POW_2_23;
            data += 3;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::INT32_BE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            const int32_t a = static_cast<int32_t>(data[0]) << static_cast<int32_t>(24);
            const int32_t b = static_cast<int32_t>(data[1]) << static_cast<int32_t>(16);
            const int32_t c = static_cast<int32_t>(data[2]) << static_cast<int32_t>(8);
            const int32_t d = static_cast<int32_t>(data[3]);
            const int32_t u = a | b | c | d;
            result[channel][sample] = static_cast<float>(u) * constant::INV_POW_2_31;
            data += 4;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::UINT8_BE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            const int8_t u = static_cast<int8_t>(static_cast<int16_t>(*data) - constant::POW_2_07_I);
            result[channel][sample] = static_cast<float>(u) * constant::INV_POW_2_07;
            data += 1;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::UINT16_BE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            const uint16_t u = static_cast<uint16_t>(data[0]) << static_cast<uint16_t>(8) | static_cast<uint16_t>(data[1]);
            const int16_t v = static_cast<int16_t>(static_cast<int32_t>(u) - constant::POW_2_15_I);
            result[channel][sample] = static_cast<float>(v) * constant::INV_POW_2_15;
            data += 2;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::UINT24_BE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            const uint32_t a = static_cast<uint32_t>(data[0]) << static_cast<uint32_t>(16);
            const uint32_t b = static_cast<uint32_t>(data[1]) << static_cast<uint32_t>(8);
            const uint32_t c = static_cast<uint32_t>(data[2]);
            const uint32_t u = a | b | c;
            const int32_t v = static_cast<int32_t>(u) - constant::POW_2_23_I;
            result[channel][sample] = static_cast<float>(v) * constant::INV_POW_2_23;
            data += 3;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::UINT32_BE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            const uint32_t a = static_cast<uint32_t>(data[0]) << static_cast<uint32_t>(24);
            const uint32_t b = static_cast<uint32_t>(data[1]) << static_cast<uint32_t>(16);
            const uint32_t c = static_cast<uint32_t>(data[2]) << static_cast<uint32_t>(8);
            const uint32_t d = static_cast<uint32_t>(data[3]);
            const uint32_t u = a | b | c | d;
            const int32_t v = static_cast<int32_t>(static_cast<int64_t>(u) - constant::POW_2_31_I);
            result[channel][sample] = static_cast<float>(v) * constant::INV_POW_2_31;
            data += 4;
        }
}

template <> void pcm::deinterleaveCastSamplesToFloatNE<pcm::Format::FLOAT32_BE>(const std::byte * data, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    for (size_t sample = 0; sample < sampleCount; ++sample)
        for (size_t channel = 0; channel < channelCount; ++channel) {
            if constexpr (std::endian::native == std::endian::little)
                for (size_t byte = 0; byte < 4; ++byte)
                    reinterpret_cast<std::byte *>(result[channel] + sample)[byte] = data[3 - byte];
            else
                for (size_t byte = 0; byte < 4; ++byte)
                    reinterpret_cast<std::byte *>(result[channel] + sample)[byte] = data[byte];
            data += 4;
        }
}

void pcm::castAnySamplesToFloatNE(const std::byte * data, Format format, uint64_t sampleCount, float * result) {
    switch (format) {
        case Format::INT8_LE: castSamplesToFloatNE<Format::INT8_LE>(data, sampleCount, result); return;
        case Format::INT16_LE: castSamplesToFloatNE<Format::INT16_LE>(data, sampleCount, result); return;
        case Format::INT24_LE: castSamplesToFloatNE<Format::INT24_LE>(data, sampleCount, result); return;
        case Format::INT32_LE: castSamplesToFloatNE<Format::INT32_LE>(data, sampleCount, result); return;
        case Format::UINT8_LE: castSamplesToFloatNE<Format::UINT8_LE>(data, sampleCount, result); return;
        case Format::UINT16_LE: castSamplesToFloatNE<Format::UINT16_LE>(data, sampleCount, result); return;
        case Format::UINT24_LE: castSamplesToFloatNE<Format::UINT24_LE>(data, sampleCount, result); return;
        case Format::UINT32_LE: castSamplesToFloatNE<Format::UINT32_LE>(data, sampleCount, result); return;
        case Format::FLOAT32_LE: castSamplesToFloatNE<Format::FLOAT32_LE>(data, sampleCount, result); return;
        case Format::INT8_BE: castSamplesToFloatNE<Format::INT8_BE>(data, sampleCount, result); return;
        case Format::INT16_BE: castSamplesToFloatNE<Format::INT16_BE>(data, sampleCount, result); return;
        case Format::INT24_BE: castSamplesToFloatNE<Format::INT24_BE>(data, sampleCount, result); return;
        case Format::INT32_BE: castSamplesToFloatNE<Format::INT32_BE>(data, sampleCount, result); return;
        case Format::UINT8_BE: castSamplesToFloatNE<Format::UINT8_BE>(data, sampleCount, result); return;
        case Format::UINT16_BE: castSamplesToFloatNE<Format::UINT16_BE>(data, sampleCount, result); return;
        case Format::UINT24_BE: castSamplesToFloatNE<Format::UINT24_BE>(data, sampleCount, result); return;
        case Format::UINT32_BE: castSamplesToFloatNE<Format::UINT32_BE>(data, sampleCount, result); return;
        case Format::FLOAT32_BE: castSamplesToFloatNE<Format::FLOAT32_BE>(data, sampleCount, result); return;
        default: return;
    }
}

void pcm::deinterleaveCastAnySamplesToFloatNE(const std::byte * data, Format format, uint64_t sampleCount, uint64_t channelCount, float * * result) {
    switch (format) {
        case Format::INT8_LE: deinterleaveCastSamplesToFloatNE<Format::INT8_LE>(data, sampleCount, channelCount, result); return;
        case Format::INT16_LE: deinterleaveCastSamplesToFloatNE<Format::INT16_LE>(data, sampleCount, channelCount, result); return;
        case Format::INT24_LE: deinterleaveCastSamplesToFloatNE<Format::INT24_LE>(data, sampleCount, channelCount, result); return;
        case Format::INT32_LE: deinterleaveCastSamplesToFloatNE<Format::INT32_LE>(data, sampleCount, channelCount, result); return;
        case Format::UINT8_LE: deinterleaveCastSamplesToFloatNE<Format::UINT8_LE>(data, sampleCount, channelCount, result); return;
        case Format::UINT16_LE: deinterleaveCastSamplesToFloatNE<Format::UINT16_LE>(data, sampleCount, channelCount, result); return;
        case Format::UINT24_LE: deinterleaveCastSamplesToFloatNE<Format::UINT24_LE>(data, sampleCount, channelCount, result); return;
        case Format::UINT32_LE: deinterleaveCastSamplesToFloatNE<Format::UINT32_LE>(data, sampleCount, channelCount, result); return;
        case Format::FLOAT32_LE: deinterleaveCastSamplesToFloatNE<Format::FLOAT32_LE>(data, sampleCount, channelCount, result); return;
        case Format::INT8_BE: deinterleaveCastSamplesToFloatNE<Format::INT8_BE>(data, sampleCount, channelCount, result); return;
        case Format::INT16_BE: deinterleaveCastSamplesToFloatNE<Format::INT16_BE>(data, sampleCount, channelCount, result); return;
        case Format::INT24_BE: deinterleaveCastSamplesToFloatNE<Format::INT24_BE>(data, sampleCount, channelCount, result); return;
        case Format::INT32_BE: deinterleaveCastSamplesToFloatNE<Format::INT32_BE>(data, sampleCount, channelCount, result); return;
        case Format::UINT8_BE: deinterleaveCastSamplesToFloatNE<Format::UINT8_BE>(data, sampleCount, channelCount, result); return;
        case Format::UINT16_BE: deinterleaveCastSamplesToFloatNE<Format::UINT16_BE>(data, sampleCount, channelCount, result); return;
        case Format::UINT24_BE: deinterleaveCastSamplesToFloatNE<Format::UINT24_BE>(data, sampleCount, channelCount, result); return;
        case Format::UINT32_BE: deinterleaveCastSamplesToFloatNE<Format::UINT32_BE>(data, sampleCount, channelCount, result); return;
        case Format::FLOAT32_BE: deinterleaveCastSamplesToFloatNE<Format::FLOAT32_BE>(data, sampleCount, channelCount, result); return;
        default: return;
    }
}

uint64_t pcm::sampleSize(Format format) {
    switch (format) {
        case Format::INT8_LE:
        case Format::INT8_BE:
        case Format::UINT8_LE:
        case Format::UINT8_BE:
            return 1;
        case Format::INT16_LE:
        case Format::INT16_BE:
        case Format::UINT16_LE:
        case Format::UINT16_BE:
            return 2;
        case Format::INT24_LE:
        case Format::INT24_BE:
        case Format::UINT24_LE:
        case Format::UINT24_BE:
            return 3;
        case Format::INT32_LE:
        case Format::INT32_BE:
        case Format::UINT32_LE:
        case Format::UINT32_BE:
        case Format::FLOAT32_LE:
        case Format::FLOAT32_BE:
            return 4;
        default:
            return 0;
    }
}

uint64_t pcm::Clip::frameSize() const {
    return mFrameSize;
}

pcm::Clip::Clip(const std::filesystem::path & path) :
    mPath{ path },
    mFile{ path, std::ifstream::in | std::ifstream::binary }
{
    if (!mFile.good()) {
        mPath = std::filesystem::path{};
        mFile = std::ifstream{};
        mMeta = Meta{};
        mDataStart = 0;
        mFrameSize = 0;
        return;
    }

    do {
        if (parseWAVE()) break;
        if (parseAIFF()) break;
        if (parseAIFC()) break;
    } while (false);

    if (mDataStart == 0) {
        mPath = std::filesystem::path{};
        mFile = std::ifstream{};
        mMeta = Meta{};
        mDataStart = 0;
        mFrameSize = 0;
    } else {
        mFrameSize = mMeta.channelCount * sampleSize(mMeta.format);
    }
}

/**
 * custom move assignment because the other object must be properly reset
 */
pcm::Clip & pcm::Clip::operator=(Clip && other) {
    if (this == &other)
        return *this;
    mPath = std::move(other.mPath);
    mFile = std::move(other.mFile);
    mMeta = std::move(other.mMeta);
    mDataStart = std::move(other.mDataStart);
    mFrameSize = std::move(other.mFrameSize);
    other.mPath = std::filesystem::path{};
    other.mFile = std::ifstream{};
    other.mMeta = Meta{};
    other.mDataStart = 0;
    other.mFrameSize = 0;
    return *this;
}

/**
 * custom move constructor because the other object must be properly reset
 */
pcm::Clip::Clip(Clip && other) :
    mPath{ std::move(other.mPath) },
    mFile{ std::move(other.mFile) },
    mMeta{ std::move(other.mMeta) },
    mDataStart{ std::move(other.mDataStart) },
    mFrameSize{ std::move(other.mFrameSize) }
{
    other.mPath = std::filesystem::path{};
    other.mFile = std::ifstream{};
    other.mMeta = Meta{};
    other.mDataStart = 0;
    other.mFrameSize = 0;
}

const std::filesystem::path & pcm::Clip::getPath() const {
    return mPath;
}

bool pcm::Clip::isGood() const {
    return !mPath.empty();
}

pcm::Meta pcm::Clip::getMeta() const {
    return mMeta;
}

uint64_t pcm::Clip::read(std::byte * data, uint64_t frameOffset, uint64_t frameCount) {
    // overflow safe checks
    if (frameOffset > mMeta.frameCount)
        return 0;
    if (mMeta.frameCount - frameOffset < frameCount)
        frameCount = mMeta.frameCount - frameOffset;
    const uint64_t startByte = mDataStart + frameOffset * mFrameSize;
    const uint64_t sizeBytes = frameCount * mFrameSize;
    mFile.seekg(startByte);
    mFile.read(reinterpret_cast<char *>(data), sizeBytes);
    // round down in case there was an error in reading file
    const uint64_t bytesRead = mFile.gcount();
    // if (bytesRead != sizeBytes) WARNING, issue reading file
    const uint64_t framesRead = bytesRead / mFrameSize;
    return framesRead;
}

void pcm::wav::save(
    const std::filesystem::path & fileName,
    const std::vector<std::vector<float>> & clip,
    uint32_t frameRate
) {
    if (clip.size() < 1) throw std::runtime_error("Empty output.");
    if (frameRate < 1) throw std::runtime_error("Sample rate zero.");
    for (const auto & channel : clip) {
        if (clip[0].size() !=channel.size())
            throw std::runtime_error("Mismatched sample counts.");
    }

    // TODO: minimize memory allocation
    std::vector<float> interleaved;
    interleaved.reserve(clip[0].size() * clip.size());
    for (std::uint64_t i = 0; i < clip[0].size(); ++i)
        for (const auto & channel : clip)
            interleaved.push_back(channel[i]);

    const std::uint16_t inBytesPerSample = 4;
    const std::uint16_t inAudioFormat = 3; // 1=>int 3=>float
    const std::uint16_t inNumberOfChannels = clip.size();
    const std::uint32_t inSampleCount = clip[0].size();

    std::ofstream file{ std::string{ fileName }, std::ofstream::binary | std::ofstream::trunc | std::ofstream::out };

    const std::uint32_t chunkSize = 36 + inSampleCount * inNumberOfChannels * inBytesPerSample;

    const std::uint32_t subchunk1Size = 16;
    const std::uint16_t audioFormat = inAudioFormat;
    const std::uint16_t numberOfChannels = inNumberOfChannels;
    const std::uint32_t sampleRate = frameRate;
    const std::uint32_t byteRate = frameRate * inNumberOfChannels * inBytesPerSample;
    const std::uint16_t blockAlign = inNumberOfChannels * inBytesPerSample;
    const std::uint16_t bitsPerSample = inBytesPerSample * 8;

    const std::uint32_t subchunk2Size = inSampleCount * inNumberOfChannels * inBytesPerSample;

    auto writeToFile = [&file] (const auto & value) {
        file.write(reinterpret_cast<const char *>(&value), sizeof(value));
        if (!file.good()) throw std::runtime_error("Error while writing file.");
    };

    // careful with this :)
    auto write4Chars = [&file] (const char * chars) {
        file.write(chars, 4);
        if (!file.good()) throw std::runtime_error("Error while writing file.");
    };

    write4Chars("RIFF");
    writeToFile(chunkSize);
    write4Chars("WAVE");

    write4Chars("fmt ");
    writeToFile(subchunk1Size);
    writeToFile(audioFormat);
    writeToFile(numberOfChannels);
    writeToFile(sampleRate);
    writeToFile(byteRate);
    writeToFile(blockAlign);
    writeToFile(bitsPerSample);

    write4Chars("data");
    writeToFile(subchunk2Size);

    file.write(reinterpret_cast<const char *>(interleaved.data()), sizeof(*interleaved.begin()) * interleaved.size());
    if (!file.good()) throw std::runtime_error("Error while writing file.");
}

bool pcm::Clip::parseAIFC() {
    // TODO: implement
    return false;
}

/**
 * FORM chunk size parameter is ignored.
 * Check if skipPast() can cause a livelock.
 */
bool pcm::Clip::parseAIFF() {
    auto read4chars = [&file = mFile] {
        std::array<char, 4> out;
        file.read(out.data(), out.size());
        if (file.gcount() != out.size())
            throw std::runtime_error("Problem with file.");
        return out;
    };
    
    auto readBigEndianInt = [&file = mFile] (int64_t length) {
        int64_t out = 0;
        std::array<char, 8> c;
        if (length > c.size())
            throw std::runtime_error("Only unsigned integer up to 8 bytes supported.");
        file.read(c.data(), length);
        if (file.gcount() != length)
            throw std::runtime_error("Problem with file.");
        // sign extend
        if (length > 0) {
            out = static_cast<char>(c[0]);
            out <<= CHAR_BIT * 7;
            out >>= CHAR_BIT * (8 - length);
        }
        for (int64_t i = 1; i < length; ++i) {
            const uint64_t tmp = static_cast<unsigned char>(c[i]);
            out |= tmp << (length - 1 - i) * CHAR_BIT;
        }
        return out;
    };
    
    auto readBigEndianUint = [&file = mFile] (uint64_t length) {
        uint64_t out = 0;
        std::array<char, 8> c;
        if (length > c.size())
            throw std::runtime_error("Only unsigned integer up to 8 bytes supported.");
        file.read(c.data(), length);
        if (file.gcount() != length)
            throw std::runtime_error("Problem with file.");
        for (uint64_t i = 0; i < length; ++i) {
            const uint64_t tmp = static_cast<unsigned char>(c[i]);
            out |= tmp << (length - 1 - i) * CHAR_BIT;
        }
        return out;
    };

    auto skipPast = [&file = mFile, &read4chars, &readBigEndianInt] (std::array<char, 4> subChunkID) {
        while (true) {
            const auto id = read4chars(); // must throw on failure!
            if (id == subChunkID) return;
            const uint32_t subchunkSize = readBigEndianInt(4);
            file.seekg(subchunkSize, std::ifstream::cur);
        }
    };

    auto readFrameRate = [&file = mFile, &readBigEndianUint] () -> uint64_t {
        std::array<char, 2> c;
        file.read(c.data(), c.size());
        if (file.gcount() != c.size())
            throw std::runtime_error("Problem with file.");
        static constexpr uint64_t MASK = ~(uint64_t(1) << uint64_t(CHAR_BIT - 1));
        const uint64_t sign = static_cast<unsigned char>(c[0]) >> (CHAR_BIT - 1);
        const uint64_t exponent = ((uint64_t(c[0]) & MASK) << uint64_t(CHAR_BIT)) | static_cast<unsigned char>(c[1]);
        const uint64_t mantissa = readBigEndianUint(8);
        const int64_t e = int64_t(exponent) - int64_t(16383);
        if (sign != 0)
            return 0;
        if (e < 0 || e > 63)
            return 0;
        const uint64_t fraction = mantissa & ((uint64_t(1) << uint64_t(63 - e)) - uint64_t(1));
        if (fraction != 0) return 0;
        const uint64_t out = mantissa >> uint64_t(63 - e);
        return out;
    };

    Meta meta;

    try {
        { 
            // read form chunk
            mFile.seekg(0);
            const auto chunkID = read4chars();
            const int32_t chunkSize = readBigEndianInt(4);
            const auto format = read4chars();
            // validate form chunk
            if (chunkID != std::array<char, 4>{ { 'F', 'O', 'R', 'M' } })
                throw std::runtime_error("Not AIFF");
            if (format != std::array<char, 4>{ { 'A', 'I', 'F', 'F' } })
                throw std::runtime_error("Not AIFF");
        }
        uint64_t bytesPerSample = 0;
        {
            // read comm subchunk
            mFile.seekg(12);
            skipPast({ { 'C', 'O', 'M', 'M' } });
            const int32_t subchunkSize = readBigEndianInt(4);
            const int16_t numberOfChannels = readBigEndianInt(2);
            const uint32_t numberOfSampleFrames = readBigEndianUint(4);
            const int16_t sampleSize = readBigEndianInt(2);
            const uint64_t frameRate = readFrameRate();
            // validate comm subchunk
            if (frameRate < 1)
                throw std::runtime_error("Unsupported AIFF:");
            if (subchunkSize != 18)
                throw std::runtime_error("Unsupported AIFF.");
            if (numberOfChannels < 1)
                throw std::runtime_error("Invalid AIFF.");
            if (numberOfSampleFrames < 1)
                throw std::runtime_error("Empty AIFF.");
            if (sampleSize < 1 || sampleSize > 32)
                throw std::runtime_error("Invalid AIFF.");
            if (sampleSize % 8 != 0)
                throw std::runtime_error("Unsupported AIFF.");
            // extract data
            if (sampleSize == 8)
                meta.format = Format::INT8_BE;
            else if (sampleSize == 16)
                meta.format = Format::INT16_BE;
            else if (sampleSize == 24)
                meta.format = Format::INT24_BE;
            else if (sampleSize == 32)
                meta.format = Format::INT32_BE;
            bytesPerSample = sampleSize / 8;
            meta.frameRate = frameRate;
            meta.frameCount = numberOfSampleFrames;
            meta.channelCount = numberOfChannels;
        }
        {
            // read ssnd subchunk
            mFile.seekg(12);
            skipPast({ { 'S', 'S', 'N', 'D' } });
            const int32_t subchunkSize = readBigEndianInt(4);
            const uint32_t offset = readBigEndianUint(4);
            const uint32_t blockSize = readBigEndianUint(4);
            const uint64_t soundDataPosition = mFile.tellg();
            mFile.seekg(0, std::ifstream::end);
            const uint64_t endPosition = mFile.tellg();
            const uint64_t available = endPosition - soundDataPosition;
            // validate ssnd subchunk
            if (endPosition < soundDataPosition)
                throw std::runtime_error("Some error.");
            if (subchunkSize < 8)
                throw std::runtime_error("Empty file.");
            if (available < subchunkSize - 8)
                throw std::runtime_error("Incomplete file.");
            if (available < offset)
                throw std::runtime_error("Incomplete file.");
            if (bytesPerSample < 1)
                throw std::runtime_error("Unsupported file");
            if (available - offset < meta.frameCount * meta.channelCount * bytesPerSample)
                throw std::runtime_error("Incomplete file.");
            if (blockSize != 0)
                throw std::runtime_error("Unsupported AIFF.");
            // extract data
            mDataStart = soundDataPosition + offset;
        }
    } catch (...) {
        mDataStart = 0;
        return false;
    }

    mMeta = meta;
    return true;
}

/**
 * WAVE chunk size parameter is ignored.
 * Check if skipPast() can cause a livelock.
 */
bool pcm::Clip::parseWAVE() {
    auto read4chars = [&file = mFile] {
        std::array<char, 4> out;
        file.read(out.data(), out.size());
        if (file.gcount() != out.size())
            throw std::runtime_error("Problem with file.");
        return out;
    };

    auto readLittleEndianUint = [&file = mFile] (uint64_t length) {
        uint64_t out = 0;
        std::array<char, 8> c;
        if (length > c.size())
            throw std::runtime_error("Only unsigned integer up to 8 bytes supported.");
        file.read(c.data(), length);
        if (file.gcount() != length)
            throw std::runtime_error("Problem with file.");
        
        for (uint64_t i = 0; i < length; ++i) {
            const uint64_t tmp = static_cast<unsigned char>(c[i]);
            out |= tmp << i * CHAR_BIT;
        }
        return out;
    };

    auto skipPast = [&file = mFile, &read4chars, &readLittleEndianUint] (std::array<char, 4> subChunkID) {
        while (true) {
            const auto id = read4chars(); // must throw on failure!
            if (id == subChunkID) return;
            const uint32_t subchunkSize = readLittleEndianUint(4);
            file.seekg(subchunkSize, std::ifstream::cur);
        }
    };

    Meta meta;

    try {
        { 
            // read riff chunk
            mFile.seekg(0);
            const auto chunkID = read4chars();
            const uint32_t chunkSize = readLittleEndianUint(4);
            const auto format = read4chars();
            // validate riff chunk
            if (chunkID != std::array<char, 4>{ { 'R', 'I', 'F', 'F' } })
                throw std::runtime_error("Not WAVE");
            if (format != std::array<char, 4>{ { 'W', 'A', 'V', 'E' } })
                throw std::runtime_error("Not WAVE");
        }
        uint64_t bytesPerSample = 0;
        { 
            // read fmt subchunk
            mFile.seekg(12);
            skipPast({ { 'f', 'm', 't', ' ' } });
            const uint32_t subchunkSize = readLittleEndianUint(4);
            const uint16_t audioFormat = readLittleEndianUint(2);
            const uint16_t numberOfChannels = readLittleEndianUint(2);
            const uint32_t sampleRate = readLittleEndianUint(4);
            const uint32_t byteRate = readLittleEndianUint(4);
            const uint16_t blockAlign = readLittleEndianUint(2);
            const uint16_t bitsPerSample = readLittleEndianUint(2);
            // validate fmt subchunk
            if (subchunkSize != 16)
                throw std::runtime_error("Unsupported WAV.");
            if (bitsPerSample == 0 || bitsPerSample % 8 != 0)
                throw std::runtime_error("Unsupported WAV.");
            if (byteRate != sampleRate * numberOfChannels * bitsPerSample / 8)
                throw std::runtime_error("Unsupported WAV.");
            if (blockAlign != numberOfChannels * bitsPerSample / 8)
                throw std::runtime_error("Unsupported WAV.");
            if (audioFormat != 1 && audioFormat != 3)
                throw std::runtime_error("Unsupported WAV.");
            // extract data
            if (audioFormat == 1) {
                if (bitsPerSample == 8)
                    meta.format = Format::UINT8_LE; // why Bill?
                else if (bitsPerSample == 16)
                    meta.format = Format::INT16_LE;
                else if (bitsPerSample == 24)
                    meta.format = Format::INT24_LE;
                else if (bitsPerSample == 32)
                    meta.format = Format::INT32_LE;
            } else if (audioFormat == 3) {
                if (bitsPerSample == 32)
                    meta.format = Format::FLOAT32_LE;
            }
            bytesPerSample = bitsPerSample / 8;
            meta.frameRate = sampleRate;
            meta.channelCount = numberOfChannels;
        }
        { 
            // read data subchunk
            mFile.seekg(12);
            skipPast({ { 'd', 'a', 't', 'a' } });
            const uint64_t subchunkSize = readLittleEndianUint(4);
            const uint64_t dataPosition = mFile.tellg();
            mFile.seekg(0, std::ifstream::end);
            const uint64_t endPosition = mFile.tellg();
            const uint64_t available = endPosition - dataPosition;
            // validate data subchunk
            if (endPosition < dataPosition)
                throw std::runtime_error("Some error.");
            if (subchunkSize < 1)
                throw std::runtime_error("Empty file.");
            if (subchunkSize < available)
                throw std::runtime_error("Incomplete file");
            if (bytesPerSample < 1)
                throw std::runtime_error("Unsupported file");
            if (subchunkSize % (bytesPerSample * meta.channelCount))
                throw std::runtime_error("Incomplete file.");
            // extract data
            meta.frameCount = subchunkSize / (bytesPerSample * meta.channelCount);
            mDataStart = dataPosition;
        }
    } catch (...) {
        mDataStart = 0;
        return false;
    }

    mMeta = meta;
    return true;
}

bool pcm::resampling::Nearest::isGood() const {
    return mInRate > 0;
}

pcm::resampling::Nearest::Nearest(uint64_t inRate, uint64_t outRate) {
    if (inRate < 1 || outRate < 1) return;
    mInRate = inRate;
    mOutRate = outRate;
}

void pcm::resampling::Nearest::resample(float * in, float * out) {
    if (mOutRate < 1) return;
    for (uint64_t i = mOutStart, end = mOutStart + mOutSize; i < end; ++i) {
        *out++ = in[i * mInRate / mOutRate - mInStart];
    }
}

std::pair<uint64_t, uint64_t> pcm::resampling::Nearest::nextRange(uint64_t outStart, uint64_t outSize) {
    if (mOutRate < 1) return { 0, 0 };
    const auto start = outStart * mInRate / mOutRate;
    mOutStart = outStart;
    mOutSize = outSize;
    uint64_t inSize = 0;
    if (outSize  < 1) {
        mInStart = start;
        inSize = 0;
        return { mInStart, inSize };
    }
    const auto last = (outStart + outSize - 1) * mInRate / mOutRate;
    mInStart = start;
    inSize = last - start + 1;
    return { mInStart, inSize };
}

uint64_t pcm::resampling::Nearest::maxRangeSize(uint64_t outSize) const {
    // TODO: verify that this is correct
    const uint64_t v = mInRate / mOutRate + (mInRate % mOutRate != 0);
    return v * outSize;
}

bool pcm::resampling::Linear::isGood() const {
    return mInRate > 0;
}

pcm::resampling::Linear::Linear(uint64_t inRate, uint64_t outRate) {
    if (inRate < 1 || outRate < 1) return;
    mInRate = inRate;
    mOutRate = outRate;
}

void pcm::resampling::Linear::resample(float * in, float * out) {
    if (mOutRate < 1) return;
    for (uint64_t i = mOutStart, end = mOutStart + mOutSize; i < end; ++i) {
        const uint64_t inPos = i * mInRate / mOutRate - mInStart;
        const float a = float(i * mInRate % mOutRate) / float(mOutRate);
        // TODO: implement buffer size validation
        *out++ = in[inPos] * (float(1) - a) + in[inPos + 1] * a;
    }
}

std::pair<uint64_t, uint64_t> pcm::resampling::Linear::nextRange(uint64_t outStart, uint64_t outSize) {
    if (mOutRate < 1) return { 0, 0 };
    const auto start = outStart * mInRate / mOutRate;
    mOutStart = outStart;
    mOutSize = outSize;
    uint64_t inSize = 0;
    if (outSize  < 1) {
        mInStart = start;
        inSize = 0;
        return { mInStart, inSize + 1 };
    }
    const auto last = (outStart + outSize - 1) * mInRate / mOutRate;
    mInStart = start;
    inSize = last - start + 1;
    return { mInStart, inSize + 1 };
}

uint64_t pcm::resampling::Linear::maxRangeSize(uint64_t outSize) const {
    // TODO: verify that this is correct
    const uint64_t v = mInRate / mOutRate + (mInRate % mOutRate != 0);
    return v * outSize + 1;
}

pcm::ClipStream::ClipStream(const std::filesystem::path & clipPath, uint64_t outRate) :
    mClip{ clipPath },
    mResampler{ mClip.getMeta().frameRate, outRate }
{
    if (!mClip.isGood() || !mResampler.isGood()) {
        mClip = Clip{};
        mResampler = resampling::Linear{};
        return;
    }

    const auto & meta = mClip.getMeta();
    mClipFormat = meta.format;
    mClipFormatByteSize = sampleSize(mClipFormat);
    mChannelCount = meta.channelCount;
}

bool pcm::ClipStream::isGood() const {
    return mClipFormat != Format::UNKNOWN;
}

uint64_t pcm::ClipStream::channelCount() const {
    return mChannelCount;
}

std::pair<uint64_t, uint64_t> pcm::ClipStream::bufferSizeRequirement(uint64_t frameCount) const {
    const auto resamplerBufferSize = mResampler.maxRangeSize(frameCount);
    const uint64_t channelCount = mChannelCount;
    const uint64_t bytesPerSample = mClipFormatByteSize;

    const uint64_t byteBufferSize = resamplerBufferSize * bytesPerSample * channelCount;
    const uint64_t floatBufferSize = resamplerBufferSize;
    return { byteBufferSize, floatBufferSize };
}

// TODO: pass an allocator instead of pointers to buffers
void pcm::ClipStream::read(float * * out, uint64_t position, uint64_t size, std::byte * byteBuffer, float * * floatBuffer) {
    const auto resamplerRequirement = mResampler.nextRange(position, size);
    const auto framesRead = mClip.read(byteBuffer, resamplerRequirement.first, resamplerRequirement.second);
    deinterleaveCastAnySamplesToFloatNE(byteBuffer, mClipFormat, framesRead, mChannelCount, floatBuffer);

    for (uint64_t channel = 0; channel < mChannelCount; ++channel)
        for (uint64_t sample = framesRead; sample < resamplerRequirement.second; ++sample)
            floatBuffer[channel][sample] = float{ 0 };

    for (uint64_t channel = 0; channel < mChannelCount; ++channel)
        mResampler.resample(floatBuffer[channel], out[channel]);
}

const std::filesystem::path & pcm::ClipStream::clipPath() const {
    return mClip.getPath();
}

pcm::Meta pcm::ClipStream::getClipMeta() const {
    return mClip.getMeta();
}
